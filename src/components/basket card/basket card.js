import React from 'react';
import './basket card.css';
import { Button,Form,Select ,Icon,InputNumber,Input,Radio} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import Nav from '../navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import {addProductToBasket,removeProductFromBasket} from '../../actions/BasketAction'




class BasketCard extends React.Component {

    

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    state = {
      value: 1,
      still:this.props.data.quantity - 1,
      count:this.props.currentCount,
      price:this.props.data.hasOffer?this.props.data.offerPrice:this.props.data.price,
      color:"red",
      sizes:"xx"
    };
  
    onChange = e => {
      console.log('radio checked', e.target.value);
      console.log('color', this.state.color);
      if(e.target.value>0 && e.target.value<=this.props.data.quantity){
        this.setState({count: e.target.value, still:this.props.data.quantity-e.target.value});
        this.props.addProductToBasket({...this.props.data,count:e.target.value})
      }
    };
    onChangeColor = (value) =>{
      this.setState({color:value})
      let color = this.state.color
      console.log(color);
      this.props.addProductToBasket({...this.props.data,selectedColor:value})
    }
    onChangeSize = (value) =>{
      this.setState({sizes:value})
      console.log(`selected ${this.state.sizes}`);
      this.props.addProductToBasket({...this.props.data,selectedSize:value})
    }

    removeProductFromDataBase = (id) => {      
      axios.delete(`${BASE_END_POINT}cart/${id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      }).then(response=>{
        console.log('removeProductFromDataBase Done');
        this.props.removeProductFromBasket(id)
      }).catch(error => {
          console.log('removeProductFromDataBase  ',error.status);
          console.log('removeProductFromDataBase  ',error.respone);
          console.log('removeProductFromDataBase  ',error);
        });
    }

    
    render() {
      const Option = Select.Option;
     const {data} = this.props
     const {still,count,price} = this.state
     console.log("COUNT   ",count)
      return ( 
        <div className='myPart animated slideInDown delay-0.5s'>
        <NavLink to={`Product/${data.id}`}>
            <img width={150} height={150} src={data.img[0]}></img>
        </NavLink>
        <div className="myPartDec">
        <p style={{display:'inline-block'}}>{data.name}</p>
        <br></br>
        <Rater rating={data.rate} total={5} interactive={false} />
        <span> (<span className='totalRate'>{data.rateNumbers}</span>)</span>
        <br></br>
        <div style={{width:'100%'}}>
          <span className='stock stockQ' style={{color:'#9ba3b6',fontWeight:400}}>{allStrings.only}  {still}  {allStrings.leftin}</span>
          
          <p className='qtySpan'>{allStrings.QTY}  </p>
          <input  style={{marginBottom: '6px'}}onChange={this.onChange} type='number' className='numberInput' value={count}></input>
        </div>
       
        <div className="dropdawnCustom">
        {data.hasColors ===true &&
          <Select 
            placeholder="Colors"
            className="form-control"
            name="colors"
            onChange={this.onChangeColor}
          >
            {data.color.map(
                val=><Option value={val}>{val}</Option>
            )}
          </Select>
          }
          {data.hasSizes ===true &&
          <Select 
            placeholder="Size"
            className="form-control"
            name="size"
            onChange={this.onChangeSize}
          >
            {data.sizes.map(
                val=><Option value={val}>{val}</Option>
            )}
          </Select>
          }
        </div>
          
        <div className='divPrice'>
        <p style={{cursor:'pointer'}} onClick={()=>{
          //this.props.removeProductFromBasket(data.id)
          this.removeProductFromDataBase(data.id)
        }} className='stock'>{allStrings.remove} </p>
        <span  className='stockPrice'>{allStrings.price} :<span className='price'>{price*count} {allStrings.EGP}</span> </span>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
  })
  
  const mapDispatchToProps = {
    addProductToBasket,
    removeProductFromBasket
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(BasketCard);
