import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import './product card.css';
import { Skeleton, message,Form, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {NavLink,Link} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import 'react-multi-carousel/lib/styles.css';
import {addProductToBasket,removeProductFromBasket} from '../../actions/BasketAction'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {getUser} from '../../actions/AuthActions'

import WOW from "wowjs";

const wow = new WOW.WOW();

class ProductCard extends React.Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
    wow.init();
    
  }
  
  state={
    selectProduct:false,
}

  fav = () => {      
      axios.post(`${BASE_END_POINT}favourite/${this.props.data.id}/products`, {}, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      }).then(response=>{
         console.log('Fav Done');
         //this.setState({favDone:true })
         const user = {...this.props.currentUser,user:{...response.data.user}}
         console.log('current user')
         console.log(user)
         this.props.getUser(user)
         localStorage.setItem('@USER', JSON.stringify(user));
      }).catch(error => {
          
          console.log('Fav error  ',error.status);
          console.log('Fav error  ',error.respone);
          console.log('Fav error  ',error);
          
        });
  }

  removeFav = () => {
        
    axios.delete(`${BASE_END_POINT}favourite/${this.props.data.id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('un Fav Done');
       //this.setState({favDone:false })
       const user = {...this.props.currentUser,user:{...response.data.user}}
       this.props.getUser(user)
       localStorage.setItem('@QsathaUser', JSON.stringify(user));
  
    }).catch(error => {

        console.log('Fav error  ',error.status);
        console.log('Fav error  ',error.respone);
        console.log('Fav error  ',error);
      });
  }


  addProductToDataBase = (id) => {      
    axios.post(`${BASE_END_POINT}cart/${id}/products`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('addProductToApi Done');
       this.props.addProductToBasket({...response.data.product,count:1,selectedColor:'',selectedSize:''})
    }).catch(error => {
        console.log('addProductToApi  ',error.status);
        console.log('addProductToApi  ',error.respone);
        console.log('addProductToApi  ',error);
      });
}

  removeProductFromDataBase = (id) => {      
    axios.delete(`${BASE_END_POINT}cart/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
      console.log('removeProductFromDataBase Done');
      this.props.removeProductFromBasket(id)
    }).catch(error => {
        console.log('removeProductFromDataBase  ',error.status);
        console.log('removeProductFromDataBase  ',error.respone);
        console.log('removeProductFromDataBase  ',error);
      });
  }
  
    render() {
        const {isRTL,data,currentUser,history,basket,go} = this.props;
        const { selectProduct } = this.state;
          
      return ( 
       
            <div className='slideItem wow flipInX'>
                  {go?
                  <div onClick={()=>{
                    this.props.history.push('/product2',{data:data.id})
                  }}>
                    <img style={{width:'100%',height:'200px',margin:'0px'}} src={data.img[0]}></img>
                 </div>
                 :
                 <NavLink to={`product/${data.id}`}>
                    <img style={{width:'100%',height:'200px',margin:'0px'}} src={data.img[0]}></img>
                 </NavLink>
                  }

                 {data.hasOffer&&
                 <div className='offer'>{data.offerRatio} %</div>
                 }
                 <div onClick={()=>{
                     if(this.props.currentUser){
                     if(basket.ids.includes(data.id)){
                        //this.props.removeProductFromBasket(data.id)
                        this.removeProductFromDataBase(data.id)
                     }else{
                        //this.props.addProductToBasket({...data,count:1})
                        this.addProductToDataBase(data.id)
                     }
                    }else{
                     history.push('/Login')
                    }
                     //this.setState({selectProduct:!selectProduct})
                 }} className='cart'>
                     <img src={basket.ids.includes(data.id)?require('../../assets/images/blueBasket.png'):require('../../assets/images/grayBasket.png')}></img>
                 </div>
                 <div onClick={()=>{
                  if(currentUser){
                    if(currentUser.user.favourite.includes(data.id)){
                        this.removeFav()
                    }else{
                        this.fav()
                    }
                  }else{
                    history.push('/Login')
                  }
                 }} 
                 className='love'>
                    {/*} <img src={currentUser?currentUser.user.favourite.includes(data.id)?require('../../assets/images/redHeart.png'):require('../../assets/images/grayHeart.png'):require('../../assets/images/grayHeart.png')}></img>
                 */}</div>
                 <div className='bottom'>
                     <p className='pname'>{data.name}</p>
                     <Rater rating={data.rate} total={5} interactive={false} />

                     <p style={{color:'#000',fontWeight: '400'}}>
                       {data.hasOffer?data.offerPrice:data.price} <span style={{float:isRTL?'left':'none',paddingRight:'2px'}}>{allStrings.EGP} </span>
                       </p>
                      {data.available ==false &&
                     <Button disabled
                     onClick={()=>{
                      if(this.props.currentUser){
                        //this.props.addProductToBasket({...data,count:1})
                        this.addProductToDataBase(data.id)
                        this.setState({selectProduct:true})
                       }else{
                        history.push('/Login')
                       }
                     }}
                     style={{borderRadius: '0px',color: 'white', backgroundColor:'#aaa',width:'100%',marginTop:'10px'}} > {allStrings.addToCart}</Button>
                      }
                      {data.available ==true &&
                     <Button 
                     onClick={()=>{
                      if(this.props.currentUser){
                        //this.props.addProductToBasket({...data,count:1})
                        this.addProductToDataBase(data.id)
                        this.setState({selectProduct:true})
                       }else{
                        history.push('/Login')
                       }
                     }}
                     style={{borderRadius: '0px',color: 'white', backgroundColor:'#3497fd',width:'100%',marginTop:'10px'}} > {allStrings.addToCart}</Button>
                      }
                 </div>
             </div>
       
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    basket: state.basket.basket
  })
  
  const mapDispatchToProps = {
    addProductToBasket,
    removeProductFromBasket,
    getUser
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(ProductCard);
