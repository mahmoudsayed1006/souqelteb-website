
import React, { Component } from 'react';
import './nav.css';
import {Icon,NavItem,Navbar} from 'react-materialize';
import { List, Avatar,Button,Dropdown,Form,Modal,Input} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import { connect } from 'react-redux';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import {logout} from '../../actions/LogoutActions'
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import {NavLink} from 'react-router-dom';
import {getNotifications,getUnreadNotificationsCount} from '../../actions/NotificationAction'




class Nav extends Component {
  state = {
    visible: false,
   
    }
    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
    handleOk = (e) => {
      //console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    handleCancel = (e) => {
     // console.log(e);
      this.setState({
        visible: false,
      });
    }

  componentDidMount() {
    console.log('cur user  ',this.props.currentUser)
  }

  readNotification = (noti) =>{
    axios.put(`${BASE_END_POINT}notif/${noti.id}/read`,null,{
        headers:{
            'Authorization':`Bearer ${this.props.currentUser.token}`
        }
    })
    .then(response=>{
        this.setState({isRead:true})
        this.props.getUnreadNotificationsCount(this.props.currentUser.token)

        //Alert.alert("READ")
    })
    .catch(error=>{
        //Alert.alert("ERROR   ",error)
    })
  }




  render() {
    const { getFieldDecorator } = this.props.form;

    const data = [
      {
        title: 'SouqElteb ',
        description:'Now 12-nov-2019',
        content:'Now if you buy anything from souqElteb you will got 10% discount'
      },
      {
        title: 'SouqElteb',
        description:'Now 12-nov-2019',
        content:'Now if you buy anything from souqElteb you will got 10% discount'
      },
      {
        title: 'SouqElteb',
        description:'Now 12-nov-2019',
        content:'Now if you buy anything from souqElteb you will got 10% discount'
      },
      {
        title: 'SouqElteb',
        description:'Now 12-nov-2019',
        content:'Now if you buy anything from souqElteb you will got 10% discount'
      },
    ];
    const menu = (
      <List
        itemLayout="horizontal"
        
        dataSource={this.props.notifications}
        renderItem={item => (
          <List.Item
          onClick={()=>{this.readNotification(item)}}
          style={{backgroundColor:item.read?'white':'#f0f1f2',}}
          >
            <List.Item.Meta
             style={{padding: "10px",color:'#000'}} 
              avatar={<Avatar src={item.resource.img.length>10?item.resource.img:require('../../assets/images/soeqprofileicon.jpg')} />}
              title={
              <a href="#">{item.resource.firstname+" "+item.resource.lastname}</a>
             
              }
              description={item.description}

            />
           
          </List.Item>
          
        )}
        
      />
    );
    return (
      <div>
          <div className='topHeader'>
              <MDBContainer> 
              <div className='nav1'>
                  <MDBRow>
                    
                    <MDBCol md="3"  xm="3"  style={{paddingTop: '11px',marginBottom: '10px'}}> 
                    <NavLink to='/'>
                    <img className='animated bounceIn delay-0.5s' height="42" src="https://res.cloudinary.com/ishabrawy/image/upload/v1576193810/goiogvmbddrapxonqvj5.png"></img>
                    </NavLink>
                    </MDBCol>
                    <MDBCol lg="6" md="5" xm="6" > 
                    <div class="input-field col s12">
                    <i class="material-icons prefix">search</i>
                    <input onKeyPress={(e)=>{
                      if (e.key === 'Enter') {
                        console.log(e.target.value)
                        this.props.history.push('/Category',{data:null,from:'search',name:e.target.value,name2:allStrings.search})
                        window.location.reload();
                      }
                    }} id="icon_prefix" type="text" class="validate" placeholder={allStrings.searchLabel}></input>
                    </div>
                    </MDBCol>
                    <MDBCol lg="3" md ="4" xm="3" >
                        <ul className='rightNav'>
                           {this.props.currentUser&&
                            <li className='downMenu animated bounceIn delay-0.5s'>
                              <Dropdown overlay={menu} trigger={['click']}>
                                <a className="ant-dropdown-link" href="#">
                              <Icon >notifications</Icon><span className="badge red">{this.props.unreadNotificationsCount}</span>
                                </a>
                              </Dropdown>
                            </li>
                           }
                            
                            <li className='animated bounceIn delay-0.6s'>
                            <NavLink to={this.props.currentUser?'/Profile':'/Login'}><Icon>person</Icon></NavLink>
                                
                            </li>
                            <li className='animated bounceIn delay-0.7s'>
                               <NavLink to={this.props.currentUser?'/Cart':'/Login'}><Icon>local_grocery_store</Icon><span className="badge red">{this.props.basket.data.length}</span></NavLink>
                            </li>
                        </ul>
                    
                    </MDBCol>
                    
                  </MDBRow>
                  </div>
                  <div className='nav2'>
                  <MDBRow>
                    
                    <MDBCol size="3"  style={{paddingTop: '7px',marginBottom: '10px'}}> 
                    <NavLink to='/'>
                    <img height="42" src="https://res.cloudinary.com/ishabrawy/image/upload/v1576193810/goiogvmbddrapxonqvj5.png"></img>
                    </NavLink>
                    </MDBCol>
                   
                    <MDBCol size="9">
                        <ul className='rightNav'>
                           
                            <li onClick={this.showModal}>
                            <Icon>search</Icon>
                                
                            </li>
                            {this.props.currentUser&&
                            <li>
                            <NavLink to={this.props.currentUser?'/Profile':'/Login'}><Icon>person</Icon></NavLink>                  
                            </li>
                            }
                            <li>
                               <NavLink to={this.props.currentUser?'/Cart':'/Login'}><Icon>local_grocery_store</Icon><span className="badge red">{this.props.basket.data.length}</span></NavLink>
                            </li>
                            <li className='downMenu'>
                              <Dropdown overlay={menu} trigger={['click']}>
                                <a className="ant-dropdown-link" href="#">
                              <Icon>notifications</Icon><span className="badge red">{this.props.unreadNotificationsCount}</span>
                                </a>
                              </Dropdown>
                            </li>

                        </ul>
                        <Modal
                        title={allStrings.search}
                        visible={this.state.visible}
                        onOk={this.handleSubmit}
                        onCancel={this.handleCancel}
                        okText={allStrings.search}
                        cancelText={allStrings.cancel}
                        >
                      
                          <Form onSubmit={this.handleSubmit} className="login-form">
                        
                            <Form.Item>
                            {getFieldDecorator('search', {
                                rules: [{ required: true, message: 'Please enter  search word' }],
                            })(
                                <Input placeholder={allStrings.searchMsg} />
                            )}
                            </Form.Item> 
                          </Form>
                        </Modal>
                    
                    </MDBCol>
                    
                  </MDBRow>
                  </div>
                  </MDBContainer>
              </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  select: state.menu.select,

  notifications: state.noti.notifications,
  refresh: state.noti.refresh,
  load: state.noti.load,
  pages: state.noti.pages,
  page: state.noti.page,
  basket: state.basket.basket,
  unreadNotificationsCount: state.noti.unreadNotificationsCount

})

const mapDispatchToProps = {
  logout,
  ChangeLanguage,
  getNotifications,
  getUnreadNotificationsCount
}

export default withRouter(connect(mapToStateProps,mapDispatchToProps)(Nav = Form.create({ name: 'normal_login' })(Nav)));

