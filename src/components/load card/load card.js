import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import './load card.css';
import { message,Form, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {NavLink} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import 'react-multi-carousel/lib/styles.css';
import {addProductToBasket,removeProductFromBasket} from '../../actions/BasketAction'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {getUser} from '../../actions/AuthActions'
import Skeleton from 'react-loading-skeleton';

import WOW from "wowjs";

const wow = new WOW.WOW();

class LoadCard extends React.Component {
  
    render() {      
      return ( 
       
            <div className='slideItem wow flipInX'>
                  <Skeleton height={200} />
                 <div className='bottom'>
                 <Skeleton height={20} />
                 <Skeleton height={20} />
                 </div>
             </div>
       
      );
    }
  }

  const mapToStateProps = state => ({
   
  })
  
  const mapDispatchToProps = {
    
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(LoadCard);
