import LanguageReducer from './LanguageReducer';
import AuthReducer from './AuthReducer';
import MenuReducer from './MenuReducer'
import { combineReducers } from "redux";
import NotificationReducer from './NotificationReducer'
import BasketReducer from './BasketReducer'


export default combineReducers({
    lang: LanguageReducer,
    auth: AuthReducer,
    menu: MenuReducer,
    noti: NotificationReducer,
    basket: BasketReducer,
});