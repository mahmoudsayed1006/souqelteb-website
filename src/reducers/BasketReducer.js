import * as types from "../actions/types"

const initialState = {
    basket:{
        data:[],
        ids:[]
    }
}

const BasketReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.ADD_PRODUCT_TO_BASKET:
            console.log('basket reducer =>'+action.payload)
            let orders={};
            
            if(state.basket.data.length==0){
                console.log('length == 0')
                orders={
                    data: [action.payload],
                    ids:  [action.payload.id]
                }
                console.log(orders)
                
            }else{
                console.log('length > 0')
                if(state.basket.ids.includes(action.payload.id)){
                    console.log("No Action")
                    const index = state.basket.ids.indexOf(action.payload.id)
                    orders = {
                        data: [...state.basket.data],
                        ids: [...state.basket.ids]
                    }
                    orders.data[index] = action.payload
                    orders.ids[index] = action.payload.id

                    console.log("updated basket   ",orders)
                }else{
                orders={
                    data: [...state.basket.data,action.payload],
                    ids:  [...state.basket.ids,action.payload.id]
                }
                }
                console.log(orders)
                
            }
            return{...state,basket:orders}

        case types.REMOVE_PRODUCT_FROM_BASKET:

            let index = state.basket.ids.indexOf(action.payload)
            console.log("index   ",index)
            state.basket.ids.splice(index,1)
            state.basket.data.splice(index,1)
            let orders2 = {
                data: [...state.basket.data],
                ids:  [...state.basket.ids]
            } 
            console.log("remove order   ",orders2)
            return{...state,basket:orders2}
        
        
        case types.CLEAR_ALL_BASKET:
            console.log('clear all ')
            const basket = {
                data:[],
                ids:[]
            }

            return {basket}

        default:
            return state;
    }

}

export default BasketReducer;