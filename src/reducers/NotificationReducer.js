import {REQUEST_TO_GET_NTIFICATIONS,UNREAD_NOTIFICATIONS_COUNT,RESPONSE_OF_NTIFICATIONS,FILED_TO_GET_NTIFICATIONS} from '../actions/types';

const initState = {
    notifications:[],
    pages:1,
    page:1,
    refresh:false,
    load:false,
    unreadNotificationsCount:0,
}

const NotificationReducer = (state=initState,action) => {
    switch(action.type){
        case REQUEST_TO_GET_NTIFICATIONS:
            return {...state,load:true}
        case RESPONSE_OF_NTIFICATIONS:
            return {...state,load:false,refresh:false,pages:action.pageCount,page:action.page, notifications:action.payload}
        case FILED_TO_GET_NTIFICATIONS:
            return {...state,load:false,refresh:false} 
        case UNREAD_NOTIFICATIONS_COUNT:
            console.log("nossss "+ action.payload)
            return { ...state,unreadNotificationsCount:action.payload };       
        default:
            return state;
    }
}

export default NotificationReducer;