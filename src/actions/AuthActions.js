import "antd/dist/antd.css";
import axios from 'axios';
import {
  LOGIN_REQUEST,LOGIN_SUCCESS,USER_TOKEN,CURRENT_USER,LOGIN_ERROR,
  SIGNUP_REQUEST,SIGNUP_SUCCESS,SIGNUP_ERROR,
   } from './types';
import { BASE_END_POINT } from '../config/URL';
import {message} from 'antd'
import  {allStrings} from '../assets/strings'


//

export function login(user,history) {
   return (dispatch) => {    
     dispatch({ type: LOGIN_REQUEST });
     //let l = message.loading('تحميل ...', 2.5)
     var msg = message.loading("ًWAIT")
     axios.post(`${BASE_END_POINT}signin`, JSON.stringify(user), {
       headers: {
         'Content-Type': 'application/json',
       },
     }).then(res => {
       console.log("user data from login  ",res.data)
       console.log("history  ",history)
       localStorage.setItem('@USER', JSON.stringify(res.data));  
       dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
       history.replace('/')  
       msg.then(() => message.success('DONE', 2.5))
     })
       .catch(error => {
        dispatch({ type: LOGIN_ERROR}); 
        console.log(error.response)
        msg.then(() => message.error(allStrings.userDataIncorrect, 2.5))
         //l.then(() => message.error('هناك خطأ فى البريد الالكترونى او كلمة المرور', 2.5))
       });
   };
 }

 export function signup(user,history) {
  return (dispatch) => {    
    dispatch({ type: SIGNUP_REQUEST });
    //let l = message.loading('تحميل ...', 2.5)
    var msg = message.loading("ًWAIT")
    axios.post(`${BASE_END_POINT}signup`, user, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      console.log("user data from signup  ",res.data)
      console.log("history  ",history)
      localStorage.setItem('@USER', JSON.stringify(res.data));  
      dispatch({ type: SIGNUP_SUCCESS, payload: res.data});  
      msg.then(() => message.success('DONE', 2.5))
      history.replace('/')  
    })
      .catch(error => {
        dispatch({ type: SIGNUP_ERROR});  
        //l.then(() => message.error('هناك خطأ فى البريد الالكترونى او كلمة المرور', 2.5))
        console.log('sginup error  ');
        console.log(error.response);
        msg.then(() => message.error(error.response.data.errors[0].msg, 2.5))
      });
  };
}

export function socialLogin(user,history) {
  return (dispatch) => {    
    dispatch({ type: SIGNUP_REQUEST });
    //let l = message.loading('تحميل ...', 2.5)
    var msg = message.loading("ًWAIT")
    axios.post(`${BASE_END_POINT}socialLogin`, user, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      console.log("user data from signup  ",res.data)
      console.log("history  ",history)
      localStorage.setItem('@USER', JSON.stringify(res.data));  
      dispatch({ type: SIGNUP_SUCCESS, payload: res.data});  
      msg.then(() => message.success('DONE', 2.5))
      history.replace('/')  
    })
      .catch(error => {
        dispatch({ type: SIGNUP_ERROR});  
        //l.then(() => message.error('هناك خطأ فى البريد الالكترونى او كلمة المرور', 2.5))
        console.log('sginup error  ');
        console.log(error.response);
        msg.then(() => message.error(error.response.data.errors[0].msg, 2.5))
      });
  };
}

 

export function getUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}


export function userToken(token){
  //console.log("hi  "+token)
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

/*export function userLocation(postion) {
  return dispatch => {
    console.log(postion)
    dispatch({type:USER_LOCATION,payload:postion})
  }
  
}
*/
