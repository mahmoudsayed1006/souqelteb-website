import {SELECT_MENU} from './types';

export function SelectMenuItem(key,select){
    return(dispatch) => {
        dispatch({type:SELECT_MENU,payload:key,isSelected:select})
    }
}