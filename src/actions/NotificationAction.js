import {RESPONSE_OF_NTIFICATIONS,UNREAD_NOTIFICATIONS_COUNT,FILED_TO_GET_NTIFICATIONS, REQUEST_TO_GET_NTIFICATIONS} from './types';
import axios from 'axios';
import {BASE_END_POINT} from '../config/URL'

export function getNotifications(page,token){
    return(dispatch) => {
        dispatch({type:REQUEST_TO_GET_NTIFICATIONS})
        var url = `${BASE_END_POINT}notif?page=${page}&limit=20`
        axios.get(url,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          }).then(response => {
            console.log("NOTI   ",response.data.data)
            const noti = response.data
            dispatch({type:RESPONSE_OF_NTIFICATIONS,pageCount:noti.pageCount,page:noti.page,payload:noti.data})
          })
            .catch(error => {
             console.log("NOTI ERROR   ",error.response)
            });
    }
}

export function getUnreadNotificationsCount(token) {
  return dispatch => {
      axios.get(`${BASE_END_POINT}notif/unreadCount`, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`  
          },
      })
      .then(response=>{
          console.log('noti count')
          console.log(response.data);
          dispatch({type:UNREAD_NOTIFICATIONS_COUNT,payload:response.data.unread})
      }).catch(error=>{
          console.log('noti count error 22')
          console.log(error.response);
      })
  }
}