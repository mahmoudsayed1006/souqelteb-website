import {
   ADD_PRODUCT_TO_BASKET,CLEAR_ALL_BASKET,REMOVE_PRODUCT_FROM_BASKET
} from './types';


export  function addProductToBasket(product) {
    console.log("basket action  ",product)
    return  (dispatch) => {
        dispatch({ type: ADD_PRODUCT_TO_BASKET,payload:product});  
    }
}

export  function removeProductFromBasket(id) {
    console.log("basket action  ",id)
    return  (dispatch) => {
        dispatch({ type: REMOVE_PRODUCT_FROM_BASKET,payload:id});  
    }
}

export  function clearBasket() {
    console.log('clear all action ')
    return  (dispatch) => {
        dispatch({ type: CLEAR_ALL_BASKET});  
    }
}