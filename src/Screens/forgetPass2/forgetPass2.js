import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './forgetPass2.css';
import {message,Form, Input, Button} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer, MDBRow} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';

class ForgetPass2 extends React.Component {


     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    

    
  
  
    render() {
      const { getFieldDecorator } = this.props.form;

          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              <div className='login'>
                <h1>{allStrings.recoveryAccount}</h1>
                <p>{allStrings.step2Desc}</p>
                <div className='theemail'>
                  <p> <i class="large material-icons eIcon">account_circle</i>mahmoudsayed1006@gmail.com</p>
                </div>
                <p style={{marginBottom: '5px',fontSize: '13px'}}>{allStrings.step2Desc2}</p>
                <p style={{fontSize: '13px'}}>mahmoudsayed1006@gmail.com</p>
                <Form onSubmit={this.handleSubmit} className="login-form">                                      
                    <Form.Item>
                    {getFieldDecorator('confirmCode', {
                        rules: [
                          { required: true, message: 'Please enter confirm code' },
                        ],
                    })(
                        <Input placeholder={allStrings.confirmeCode} />
                    )}
                    </Form.Item>
                </Form>
                <NavLink to='/ForgetPasswordStep3'>
                   <Button className='nextBtn' onClick={this.handleSubmit}>{allStrings.next}</Button>
                </NavLink>
              </div>
            </MDBRow>
        
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(ForgetPass2 = Form.create({ name: 'normal_login' })(ForgetPass2));
