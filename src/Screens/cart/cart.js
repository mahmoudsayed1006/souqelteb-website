import React from 'react';
import './cart.css';
import { Button,Form,Select ,Icon,InputNumber,message, Input,Radio} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import BasketCard from '../../components/basket card/basket card'
import {clearBasket,addProductToBasket,} from '../../actions/BasketAction'



class Cart extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      this.getCities()
      this.getProductsFromDataBase()
      
    }
    state = {
      value: 2,
      flag: 1,
      total:0,
      citiies: [],
      areas: [],
      payType: 'cash',
      //address:null,
      city:null,
      area:null,

      firstName:' ',
      lastName:' ',
      phone:' ',
      address:' ',
      loading:true,
      prices:[]
    };
  
    onChange = e => {
      console.log('radio checked', e.target.value);
      this.setState({value: e.target.value,});
    };
    
    step1 = () => {
      const flag = 1;
      this.setState({ flag });
    }

  
    step2 = ()  =>{
     const flag = 2;
      this.setState({ flag });
    }
    step3 = ()  =>{
      const flag = 3;
      this.setState({ flag });
    }
    step4 = ()  =>{
      const flag = 4;
      this.setState({ flag });
    }

    calculatePrice = (data) =>{
      let price=0;
      data.map(val=>{
        const p = val.hasOffer?val.offerPrice:val.price
        price=price+(p*val.count)
        console.log("Calc count  ",val.count)
        console.log("Calc price  ",val.p)
      })

      //this.setState({total:price})
      console.log("TOTAL  ",price)
      return price
    }

    getProductsFromDataBase = () => {
      axios.get(`${BASE_END_POINT}cart/${this.props.currentUser.user.id}/users`)
      .then(response=>{
        console.log('PRoducts from database  ',response.data)
        response.data.data.map(p=>{
          console.log("P    ",p)
           this.setState({loading:false,})
          this.props.addProductToBasket({...p.product,count:1})
        })
      })
      .catch(error=>{
        this.setState({loading:false,})
        console.log('PRoducts from database ERROR  ',error)
        console.log('PRoducts from database ERROR  ',error.respose)
      })
    }

    
    getCities = () => {
      axios.get(`${BASE_END_POINT}cities`)
      .then(response=>{
        console.log('CITIES  ',response.data)
        this.setState({citiies:response.data})
        this.getAreas(response.data[0].id)
      })
      .catch(error=>{
        console.log('CITIES ERROR  ',error)
        console.log('CITIES ERROR  ',error.respose)
      })
    }

    getAreas = (areaId) => {
      axios.get(`${BASE_END_POINT}cities/${areaId}/areas`)
      .then(response=>{
        console.log('AREAS  ',response.data)
        this.setState({areas:response.data})
      })
      .catch(error=>{
        console.log('AREAS ERROR  ',error)
        console.log('AREAS ERROR  ',error.respose)
      })
    }

    orderOwnerInfo = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['firstname','lastname','phone','area','city','address'],(err, values)  => {
        if (!err) {
         /* const data = {
            firstname:values.firstname,
            lastname:values.lastname,
            //email: values.email,
            phone:values.phone,
            area:values.area.key,
            city:values.city.key,
            adress: values.address,
          }*/
        
          console.log("VALUES   ",values)
        }
      });
    };
  
    delivaryCost = () => {
      const {city,area} = this.state
      const { currentUser, isRTL } = this.props
      const orders = []
     
      this.props.basket.data.map(val=>{
        orders.push({
          product:val.id,
          count:val.count,
         
        })
      })
      

      const fullOrder = {
        productOrders: orders,
        city: city,
        area:area,
      }
      if(area&&city){
       axios.post(`${BASE_END_POINT}orders/getDelivaryPrice`,JSON.stringify(fullOrder),{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${currentUser.token}`
        },
       })
       .then(response=>{
        console.log(response.data)
        this.setState({prices:response.data})
       })
       .catch(error=>{
         console.log("ERROR  ",error)
         console.log("ERROR  ",error.response)
       })
      }else {
        console.log(' ERROR  ')
      }
    }

    requestOrder = () => {
      const {value,address,city,area} = this.state
      const { currentUser, isRTL } = this.props
      const orders = []
      console.log(this.props.basket.data)
      this.props.basket.data.map(val=>{
        console.log("MY VALLLLLLLL    ",val)
        orders.push({
          product:val.id,
          count:val.count,
          sizes:val.selectedSize,
          color:val.selectedColor,
        })
      })
      console.log(...orders)

      const fullOrder = {
        destination:[1.3,1.5],
        productOrders: orders,
        city: city,
        address: address,
        area:area,
        paymentSystem: value==2?'cash': 'visa'
      }

      console.log("MY ORDERS   ",orders)
      console.log("MY FULL ORDERS   ",fullOrder)

      if(address&&city){
       axios.post(`${BASE_END_POINT}orders/${currentUser.user.id}/users`,JSON.stringify(fullOrder),{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${currentUser.token}`
        },
       })
       .then(response=>{
        message.success('done order');
        this.props.clearBasket()
        this.setState({flag:4})
       })
       .catch(error=>{
         console.log("ERROR  ",error)
         console.log("ERROR  ",error.response)
         message.error(this.props.isRTL?allStrings.countError:allStrings.countError);
       })
      }else {
        message.error(this.props.isRTL?allStrings.completeValid:allStrings.completeValid );
      }
    }
    
   
    render() {
      console.log(this.props.basket.data.length)
      console.log(this.state.loading==false)

      const { getFieldDecorator } = this.props.form;
      function onChange(value) {
        console.log('changed', value);
      }
      const { flag,citiies, areas,value,firstName,lastName,phone,address, city } = this.state;
      const {basket,isRTL} = this.props
      const Option = Select.Option;
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
              {this.props.basket.data.length==0&&flag==1&&
              <MDBRow>
              <div className='result'>
              <h1>{allStrings.Thank}</h1> 
              <h1>{allStrings.emptyPasket}</h1>
              <br></br>
              <Button onClick={()=>{
                 this.props.history.push('/')
              }} className='btnResult' >{allStrings.Backto}</Button>
              </div>
              </MDBRow>
              }
              
            {flag==1&&this.props.basket.data.length>0&&this.state.loading==false&&
              <MDBRow>
                  <MDBCol md="12" lg="8" >
                      <div className='cartTop'>
                        <ul>
                          <li style={{color:'#000'}} onClick={this.step1}>{allStrings.yourCart}</li>
                          <li onClick={this.step2}>{allStrings.shipping}</li>
                          <li onClick={this.step3}>{allStrings.payment}</li>
                        </ul>
                        {basket.data.map(product=>
                        <BasketCard currentCount={product.count} data={product} />
                        )}       
                       
                      </div>
                  </MDBCol>
  
                  <MDBCol md="12" lg="4" className='animated bounceIn delay-0.5s'>
                      <div className='bill'>
                          <p style={{fontWeight: '600',fontSize: '16px'}}>{allStrings.cartTotal}</p>
                          <br></br>
                          <span className='spanLeft'>{allStrings.totalCost} <span style={{float:'right',color:'#000'}} className='price'>{this.calculatePrice(this.props.basket.data)} EGP</span></span>
                          <br></br>
                           {/*
                          <span className='spanLeft'>{allStrings.delivaryCost} <span style={{float:'right',color:'#000'}} className='price'>200 EGP</span></span>
                          <br></br>
                          
                          <span className='spanLeft'>{allStrings.finalCost} <span  style={{float:'right',color:'#000'}}className='price'>{this.calculatePrice(basket.data)} EGP</span></span>
                        <br></br>*/}
                          <Button className='addBtn' onClick={this.step2}>{allStrings.ProceedToShipping}</Button>

                      </div>
                  </MDBCol>  
              </MDBRow> 
              } 

              {flag==2&&this.props.basket.data.length>0&&
              <MDBRow>
                  <MDBCol md="12" lg="8">
                  <div className='cartTop cartStep2'>
                        <ul>
                          <li onClick={this.step1}>{allStrings.yourCart}</li>
                          <li style={{color:'#000'}} onClick={this.step2}>{allStrings.shipping}</li>
                          <li onClick={this.step3}>{allStrings.payment}</li>
                        </ul>
                    <Form onSubmit={this.orderOwnerInfo}  className="login-form animated fadeInLeft delay-0.5s" style={{width: '70%',marginLeft:'6%'}}>  
                   
                   <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'12px' }}>
                    {getFieldDecorator('firstname', {
                        rules: [
                          { required: false, message: 'Please enter first name' },
                        
                        ],
                        initialValue:firstName.replace(/\s/g, '').length?firstName:null
                    })(
                        <Input onChange={(e)=>{
                          console.log("fn val  ",e.target.value)
                          this.setState({firstName:e.target.value})
                        }} placeholder={allStrings.firstname} />
                    )}
                    </Form.Item>
                    
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'4px' }}>
                    {getFieldDecorator('lastname', {
                        rules: [
                          { required: false, message: 'Please enter last name' },
                        ],
                        initialValue:lastName.replace(/\s/g, '').length?lastName:null
                    })(
                        <Input onChange={(e)=>{
                          console.log("fn val  ",e.target.value)
                          this.setState({lastName:e.target.value})
                        }} placeholder={allStrings.lastname} />
                    )}
                    </Form.Item> 
                                                     
                    <Form.Item>
                    {getFieldDecorator('address', {
                        rules: [
                          { required: true, message: 'Please enter your address' },
                          
                        ],
                        initialValue:address.replace(/\s/g, '').length?address:null
                    })(
                        <Input onChange={(e)=>{
                          console.log("address val  ",e.target.value)
                          this.setState({address:e.target.value})
                        }} placeholder={allStrings.address} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('city', {
                        rules: [{ required: true, message: 'Please enter city' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.city}
                      onChange={(value)=>{
                        console.log("city val  ",value.key)
                        this.getAreas(value.key)
                        this.setState({city:value.key})
                      }}
                      style={{ width: '100%'}} >
                        {citiies.map(city=>(
                          <Option value={city.id}>{isRTL?city.arabicCityName:city.cityName} </Option>
                        ))}                     
                      </Select>
                    )}
                    </Form.Item>
                    
                    <Form.Item>
                    {getFieldDecorator('area', {
                        rules: [{ required: false, message: 'Please enter area' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.area}
                      style={{ width: '100%'}} 
                      onChange={(value)=>{
                        console.log("city val  ",value.key)
                        this.getAreas(value.key)
                        this.setState({area:value.key})
                      }}
                      >
                          
                          {areas.map(area=>(
                            <Option value={area.id}>{isRTL?area.arabicAreaName:area.areaName} </Option>
                          ))} 
                                      
                      </Select>
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'12px' }}>
                    {getFieldDecorator('phone', {
                        rules: [
                          { required: false, message: 'Please enter phone ,' },
                          //{type: 'number',  message: 'Please enter numbers' }
                        ],
                        initialValue:phone.replace(/\s/g, '').length?phone:null
                    })(
                        <Input onChange={(e)=>{
                          console.log("fn val  ",e.target.value)
                          this.setState({phone:e.target.value})
                        }} placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                    
                  </Form>
                  </div>
                  </MDBCol>
  
                  <MDBCol md="12" lg="4" className='animated bounceIn delay-0.5s'>
                      <div className='bill'>
                          <p style={{fontWeight: '600',fontSize: '16px'}}>{allStrings.cartTotal}</p>
                          <br></br>
                          <span className='spanLeft'>{allStrings.totalCost} <span style={{float:'right',color:'#000'}} className='price'>{this.calculatePrice(this.props.basket.data)} EGP</span></span>
                          <br></br>
                          {/*
                         <span className='spanLeft'>{allStrings.delivaryCost} <span style={{float:'right',color:'#000'}} className='price'>200 EGP</span></span>
                          <br></br>
                           
                          <span className='spanLeft'>{allStrings.finalCost}<span  style={{float:'right',color:'#000'}}className='price'>2,300 EGP</span></span>
                          <br></br>*/}
                          <Button className='addBtn'  onClick={()=>{
                             this.step3()
                             this.delivaryCost()
                          }} >{allStrings.ProceedToShipping}</Button>

                      </div>
                  </MDBCol>  
              </MDBRow> 
              } 
               {flag == 3&&this.props.basket.data.length>0&&
              <MDBRow>
                  <MDBCol md="12" lg="8">
                  <div className='cartTop cartStep2'>
                        <ul>
                          <li onClick={this.step1}>{allStrings.yourCart}</li>
                          <li onClick={this.step2}>{allStrings.shipping}</li>
                          <li style={{color:'#000'}} onClick={this.step3}>{allStrings.payment}</li>
                        </ul>
                    <Form onSubmit={this.handleSubmit2} className="login-form animated slideInUp delay-0.5s" style={{width: '70%',marginLeft:'6%'}}>  
                    <Form.Item>
                      <Radio.Group onChange={this.onChange} value={this.state.value}>
                        <Radio value={1} disabled>{allStrings.creditCard}</Radio>
                        <Radio value={2}>{allStrings.cashDelivery}</Radio>
                        
                      </Radio.Group>  
                     
                    </Form.Item>                 
                    {value==1&&
                    <div>
                    <Form.Item>

                    {getFieldDecorator('cardName', {
                        rules: [
                          { required: true, message: 'Please enter your card Name' },
                          
                        ],
                    })(
                        <Input placeholder={allStrings.cardName} />
                    )}
                    </Form.Item>   
                    <Form.Item>
                    {getFieldDecorator('cardNumber', {
                        rules: [
                          { required: true, message: 'Please enter card Number ,' },
                          {type: 'number',  message: 'Please enter numbers' }
                        ],
                    })(
                        <Input placeholder={allStrings.CardNumber}  />
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'12px' }}>
                    {getFieldDecorator('expiration', {
                        rules: [
                          { required: true, message: 'Please enter Expiration Date' },
                        
                        ],
                    })(
                        <Input placeholder={allStrings.ExpirationDate} />
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'4px' }}>
                    {getFieldDecorator('year', {
                        rules: [
                          { required: true, message: 'Please enter year' },
                        ],
                    })(
                        <Input placeholder={allStrings.year} />
                    )}
                    </Form.Item>     
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'4px' }}>
                    {getFieldDecorator('SecurityCode', {
                        rules: [
                          { required: true, message: 'Please enter Security Code' },
                        ],
                    })(
                        <Input placeholder={allStrings.SecurityCode} />
                    )}
                    </Form.Item>       
                    </div>
                    }
                 </Form>
                  </div>
                  </MDBCol>
  
                  <MDBCol md="12" lg="4" className='animated bounceIn delay-0.5s'>
                      <div className='bill'>
                          <p style={{fontWeight: '600',fontSize: '16px'}}>{allStrings.cartTotal}</p>
                          <br></br>
                          <span className='spanLeft'>{allStrings.totalCost} <span style={{float:'right',color:'#000'}} className='price'>{this.calculatePrice(this.props.basket.data)} {allStrings.EGP}</span></span>
                          <br></br>
                          <span className='spanLeft'>{allStrings.delivaryCost} <span style={{float:'right',color:'#000'}} className='price'>{this.state.prices.delivaryCost} {allStrings.EGP}</span></span>
                          <br></br>
                           
                          <span className='spanLeft'>{allStrings.finalCost}<span  style={{float:'right',color:'#000'}}className='price'>{this.state.prices.finalTotal} {allStrings.EGP}</span></span>
                          <br></br>
                          <Button className='addBtn' onClick={()=>{
                             this.requestOrder()
                          }}>{allStrings.PlaceOrder}</Button>

                      </div>
                  </MDBCol>  
              </MDBRow> 
              } 
              {flag == 4&&
               <MDBRow>
                 <div className='result'>
                 <h1>{allStrings.Thank}</h1> 
                 <h1>{allStrings.orderWay}</h1>
                 <br></br>
                 <Button onClick={()=>{
                 this.props.history.push('/')
              }} className='btnResult' >{allStrings.Backto}</Button>
                 </div>
                
               </MDBRow>
              }
               
              
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    flagUser: state.auth.flagUser,
    select: state.menu.select,
    basket: state.basket.basket,
    currentUser: state.auth.currentUser,
  })
  
  const mapDispatchToProps = {
    clearBasket,
    addProductToBasket,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Cart = Form.create({ name: 'normal_login' })(Cart));
