import React ,{ Fragment }from 'react';


import './sign up.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm,Steps,Select ,Icon} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {signup} from '../../actions/AuthActions'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
const { Step } = Steps;
const { TextArea } = Input;

let user = new FormData();

class SignUp extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
  state = {
    current: 1,
    citiies: [],
    areas: [],
    firstName:' ',
    lastName:' ',
    email:' ',
    phone:' ',
    password:' ',
    confirmPassword:' ',
    address:' ',
  };

  onChange = current => {
    console.log('onChange:', current);
    this.setState({ current });
  };


     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      this.getCities()
    }
    

    
  
    next = () => {
      const current = 2;
      this.setState({ current });
    }
  
    prev = ()  =>{
      const current = 1;
      this.setState({ current });
    }

    getCities = () => {
      axios.get(`${BASE_END_POINT}cities`)
      .then(response=>{
        console.log('CITIES  ',response.data)
        this.setState({citiies:response.data})
        this.getAreas(response.data[0].id)
      })
      .catch(error=>{
        console.log('CITIES ERROR  ',error)
        console.log('CITIES ERROR  ',error.respose)
      })
    }

    getAreas = (areaId) => {
      axios.get(`${BASE_END_POINT}cities/${areaId}/areas`)
      .then(response=>{
        console.log('AREAS  ',response.data)
        this.setState({areas:response.data})
      })
      .catch(error=>{
        console.log('AREAS ERROR  ',error)
        console.log('AREAS ERROR  ',error.respose)
      })
    }

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {

         if(this.state.current==1){
         console.log("FROM STEP 1   ",values)
         user.append('firstname',values.firstname)
         user.append('lastname',values.lastname)
         user.append('email',values.email)
         user.append('phone',values.phone)
         user.append('signUpFrom','normal')
         user.append('type','CLIENT')
         user.append('token','')
         user.append('password',values.password)
         this.next()
         }

         if(this.state.current==2){
         console.log("FROM STEP 2   ",values)
         user.append('area',values.area.key)
         user.append('city',values.city.key)
         user.append('address',values.address)

         user.forEach(val=>{
           console.log("USER VALUES   ",val)
         })
         this.props.signup(user,this.props.history)
         }
        }
        
      });
    };
  
  

    render() {
      const { getFieldDecorator } = this.props.form;
      const { current, citiies, areas ,firstName,lastName,email,phone,address,password,confirmPassword} = this.state;
      const {isRTL} = this.props
      const Option = Select.Option;

          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              
              <div className='signUp login'>

              <Form onSubmit={this.handleSubmit} className="login-form">  
              {current==1&&
              
              <div>
                <div className='signUpTitle'>
                <span className='leftTitle' style={{color:'#3497fd'}}>{allStrings.BasicInfo}</span>
                <span className='rightTitle2'>{allStrings.LocationInfo}</span>
                </div>

                <div className='signUpIcon'>
                  <button style={{backgroundColor:'transparent',borderWidth:0,margin:'-6px'}} onClick={this.prev} >
                  <span className='leftIcon' style={{background:'#3497fd',border: '1px solid #3497fd'}}><Icon type="check" /></span>
                  </button>

                  <hr className='iconHr2' style={{borderTop: '1.5px solid #3497fd'}}></hr>

                  <button style={{backgroundColor:'transparent',borderWidth:0,margin:'-6px'}} onClick={this.next} >
                    <span className='rightIcon2'>2</span>
                  </button>
              </div>
                <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'12px' }}>
                {getFieldDecorator('firstname', {
                    rules: [
                      { required: true, 
                        message: isRTL?allStrings.firstnameValid:allStrings.firstnameValid      

                       },
                    ],
                    initialValue:firstName.replace(/\s/g, '').length?firstName:null
                })(
                    <Input onChange={(e)=>{
                       this.setState({firstName:e.target.value})
                    }} placeholder={allStrings.firstname} />
                )}
                </Form.Item>
                <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'4px' }}>
                {getFieldDecorator('lastname', {
                    rules: [
                      { required: true, 
                        message: isRTL?allStrings.lastnameValid:allStrings.lastnameValid                         
                      },
                    ],
                    initialValue:lastName.replace(/\s/g, '').length?lastName:null
                })(
                    <Input onChange={(e)=>{
                      this.setState({lastName:e.target.value})
                   }} placeholder={allStrings.lastname} />
                )}
                </Form.Item>                                    
                <Form.Item>
                {getFieldDecorator('email', {
                    rules: [
                      { required: true, message: isRTL?allStrings.emailValid:allStrings.emailValid},
                      {type: 'email',  message: 'صيغة البريد الالكترونى غير صحيحة' }
                    ],
                    initialValue:email.replace(/\s/g, '').length?email:null
                })(
                    <Input
                    onChange={(e)=>{
                      this.setState({email:e.target.value})
                   }} 
                     placeholder={allStrings.email} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('password', {
                    rules: [{ required: true,  message: isRTL?allStrings.passwordValid:allStrings.passwordValid }],
                    initialValue:password.replace(/\s/g, '').length?password:null
                })(
                    <Input.Password 
                    onChange={(e)=>{
                      this.setState({password:e.target.value})
                   }}
                     placeholder={allStrings.password} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('confirmPassword', {
                    rules: [{ required: true, message: isRTL?allStrings.confirmPasswordValid:allStrings.confirmPasswordValid}],
                    initialValue:confirmPassword.replace(/\s/g, '').length?confirmPassword:null
                })(
                    <Input.Password 
                    onChange={(e)=>{
                       this.setState({confirmPassword:e.target.value})
                    }}
                    placeholder={allStrings.password} />
                )}
                </Form.Item> 
                <Form.Item>
                {getFieldDecorator('phone', {
                    rules: [
                      { required: true, message: isRTL?allStrings.phoneValid:allStrings.phoneValid },
                      //{type: 'number',  message: 'Please enter numbers' }
                    ],
                    initialValue:phone.replace(/\s/g, '').length?phone:null
                })(
                    <Input onChange={(e)=>{
                      this.setState({phone:e.target.value})
                   }} placeholder={allStrings.phone} />
                )}
                </Form.Item>

              </div>   
              }


              {current==2&&
              <div>
                <div className='signUpTitle'>
                <span className='leftTitle' >{allStrings.BasicInfo}</span>
                <span className='rightTitle2'>{allStrings.LocationInfo}</span>
                </div>

                <div className='signUpIcon'>
                  <button style={{backgroundColor:'transparent',borderWidth:0,margin:'-6px'}} onClick={this.prev} >
                  <span className='leftIcon'><Icon type="check" /></span>
                  </button>

                  <hr className='iconHr2' style={{borderTop: '1.5px solid #21d366'}}></hr>

                  <button style={{backgroundColor:'transparent',borderWidth:0,margin:'-6px'}} onClick={this.next} >
                    <span className='rightIcon2'>2</span>
                  </button>
              </div>
                <Form.Item>
              {getFieldDecorator('city', {
                  rules: [{ required: true,  message: isRTL?allStrings.cityValid:allStrings.cityValid }],
              })(
                <Select labelInValue  
                placeholder={allStrings.city}
                style={{ width: '100%'}} 
                onChange={(value)=>this.getAreas(value.key)}
                >
                
                {citiies.map(city=>(
                  <Option value={city.id}>{isRTL?city.arabicCityName:city.cityName} </Option>
                ))}                    
                </Select>
              )}
              </Form.Item>
                <Form.Item>
                {getFieldDecorator('area', {
                    rules: [{ required: true, message: isRTL?allStrings.areaValid:allStrings.areaValid  }],
                })(
                  <Select labelInValue  
                  placeholder={allStrings.area}
                  style={{ width: '100%'}} >
                    {areas.map(area=>(
                      <Option value={area.id}>{isRTL?area.arabicAreaName:area.areaName} </Option>
                    ))}       
                  </Select>
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('address', {
                    rules: [{ required: true, message: isRTL?allStrings.addressValid:allStrings.addressValid }],
                    initialValue:address.replace(/\s/g, '').length?address:null
                })(
                    <TextArea onChange={(e)=>{
                      this.setState({address:e.target.value})
                   }} rows={4} placeholder={allStrings.address} />
                )}
                </Form.Item>
                <p style={{textAlign:'center', color:'#000',marginTop:'30px'}}>
                <NavLink to='/Login'>i have an account sign in</NavLink>
                <hr></hr>
                </p>
              </div>
              
              }
                </Form>
                <Button className='loginBtn' onClick={this.handleSubmit}>{current==1?allStrings.next:allStrings.SignUp}</Button>
               
                               
              </div>
            </MDBRow>
        
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    signup,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(SignUp = Form.create({ name: 'normal_login' })(SignUp));
