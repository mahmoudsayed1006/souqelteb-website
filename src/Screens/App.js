import React, { Component } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import Home from './home/home';
import Login from './login/login';
//import SignUp from './sign up/sign up';
import SignUp from './sign up step2/sign up';
import ForgetPass1 from './forgetPass1/forgetPass1';
import ForgetPass2 from './forgetPass2/forgetPass2';
import ForgetPass3 from './forgetPass3/forgetPass3';
import Product from './product/product';
import Product2 from './product2/product2';
import Category from './category/category';
import Cart from './cart/cart';
import Profile from './profile/profile';
import ContactUs from './contact/contact'
import About from './about/about'
import Usage from './usage/usage'
import Terms from './terms/terms'

class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
          <Route exact path="/" component={Home} />
            <Route path='/Login' component={Login}/>
            <Route path='/SignUp' component={SignUp}/>
            <Route path='/ForgetPasswordStep1' component={ForgetPass1}/>
            <Route path='/ForgetPasswordStep2' component={ForgetPass2}/>
            <Route path='/ForgetPasswordStep3' component={ForgetPass3}/>
            <Route path='/Product/:id' component={Product}/>
            <Route path='/Category' component={Category}/>
            <Route path='/Cart' component={Cart}/>
            <Route path='/Profile' component={Profile}/>
            <Route path='/ContactUs' component={ContactUs}/>
            <Route path='/AboutUs' component={About}/>
            <Route path='/Usage' component={Usage}/>
            <Route path='/Terms' component={Terms}/>
            <Route path='/product2' component={Product2}/>
          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
