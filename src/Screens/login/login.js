import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import './login.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm, Alert} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput } from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import {NavLink} from 'react-router-dom';
import {login,signup,socialLogin} from '../../actions/AuthActions'
import Loading from '../../components/loading/loading'
import { ClapSpinner } from "react-spinners-kit";




class Login extends React.Component {

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    responseGoogle = (response) => {
      console.log(response);
      const user ={
        firstname:response.profileObj.givenName,
        lastname:response.profileObj.familyName?response.profileObj.familyName:'',
        email:response.profileObj.email,
        signUpFrom:'social',
      }

      this.props.socialLogin(user,this.props.history)
    }

    responseFacebook = (response) => {
      console.log("facebook   ",response);
      const splitName = response.name.split(' ')
      const user ={
        firstname:splitName[0],
        lastname:splitName[1]?splitName[1]:'',
        email:response.email,
        signUpFrom:'social',
      }
      /*const user = new FormData()
      user.append('firstname',splitName[0])
      user.append('lastname',splitName[1])
      user.append('email',response.email)*/

      /*user.append('phone','')
      user.append('signUpFrom','facebook')
      user.append('type','CLIENT')
      user.append('token','')
      user.append('address','')*/
      this.props.socialLogin(user,this.props.history)
    }


    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const user = {
            email: values.email,
            password: values.password,
            token:''
          }
          this.props.login(user,this.props.history)
        }
      });
    };
    
  
    render() {
      const { getFieldDecorator } = this.props.form;
      const {isRTL} = this.props
          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              <div className='login'>
                
                <Form onSubmit={this.handleSubmit} className="login-form">                                      
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: isRTL?allStrings.emailValid:allStrings.emailValid},
                          {type: 'email',  message: 'صيغة البريد الالكترونى غير صحيحة' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true,  message: isRTL?allStrings.passwordValid:allStrings.passwordValid }],
                    })(
                        <Input.Password placeholder={allStrings.password} />
                    )}
                    </Form.Item>
                </Form>

                <p style={{textAlign:'right', color:'#000'}}>
                <NavLink to='/ForgetPasswordStep1'>{allStrings.forgetPassword}</NavLink>
                </p>
                
                <Button className='loginBtn' onClick={this.handleSubmit}>{allStrings.login}</Button>
                
                <h5>Or</h5>
                <div className="social">
                  <FacebookLogin
                      appId="2339544562816821"
                      autoLoad={false}
                      fields="name,email,picture"
                      callback={this.responseFacebook}
                      icon="fa-facebook" 
                      textButton={allStrings.faceLogin}
                  />
                  <GoogleLogin style={{width:"100%"}}
                      clientId="5703585507-f7dpadens130nckp4pejc17m86qahk0v.apps.googleusercontent.com"
                      
                      buttonText={allStrings.gmailLogin}
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogle}
                      cookiePolicy={'single_host_origin'}
                  />
                  <p style={{textAlign:'center', color:'#000',marginTop:'30px'}}>
                    <NavLink to='/SignUp'>{allStrings.noAccountSignUp}</NavLink>
                <hr></hr>
                </p>
                </div>

               
              </div>
            
            </MDBRow>
            </MDBContainer>
            
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    loading: state.auth.loading,
    //currentUser: state.auth.currentUser,
    //select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    login,
    signup,
    socialLogin
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Login = Form.create({ name: 'normal_login' })(Login));
