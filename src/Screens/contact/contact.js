import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import './contact.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm, Alert} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput } from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';

import {NavLink} from 'react-router-dom';
import {login,signup} from '../../actions/AuthActions'
import Loading from '../../components/loading/loading'
import { ClapSpinner } from "react-spinners-kit";
const { TextArea } = Input;


class Contact extends React.Component {
 
  componentDidMount() {
    window.scrollTo(0, 0)
  }
     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    
    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          var msg = message.loading("ًWAIT")
          const data = {
            name:values.firstname+" "+values.lastname,
            email: values.email,
            number: values.phone,
            message: values.message,
          }
          axios.post(`${BASE_END_POINT}contact-us`,data,{
            headers:{
              'Content-Type':'application/json'
            }
          })
          .then(response=>{
            msg.then(() => message.success('DONE', 2.5))
            this.props.history.replace('/')
          })
          .catch(error=>{
            console.log("Error    ",error.response)
            msg.then(() => message.error('ERROR', 2.5))
          })
        }
      });
    };


  
    render() {
      const { getFieldDecorator } = this.props.form;
      const {isRTL} = this.props
          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              <div className='login contact'>
                
                <Form onSubmit={this.handleSubmit} className="login-form">                                      
                    

                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'20px' }}>
                    {getFieldDecorator('firstname', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.firstnameValid:allStrings.firstnameValid      
                          },
                        
                        ],
                    })(
                        <Input placeholder={allStrings.firstname} />
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'0px' }}>
                    {getFieldDecorator('lastname', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.lastnameValid:allStrings.lastnameValid      

                          },
                        ],
                    })(
                        <Input placeholder={allStrings.lastname} />
                    )}
                    </Form.Item>   
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.emailValid:allStrings.emailValid      

                           },
                          {type: 'email',  message: 'incorrect email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.phoneValid:allStrings.phoneValid      

                          },
                          //{type: 'number',  message: 'Please enter numbers' }
                        ],
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('message', {
                        rules: [{ required: true, 
                          message: isRTL?allStrings.messageValid:allStrings.messageValid      

                        }],
                    })(
                        <TextArea rows={4} placeholder={allStrings.message} />
                    )}
                    </Form.Item>

                </Form>

                <Button className='loginBtn' onClick={this.handleSubmit}>{allStrings.send}</Button>
               
              </div>
            
            </MDBRow>
            </MDBContainer>
            
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    loading: state.auth.loading,
    //currentUser: state.auth.currentUser,
    //select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    login,
    signup,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Contact = Form.create({ name: 'normal_login' })(Contact));
