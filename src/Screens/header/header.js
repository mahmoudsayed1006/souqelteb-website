import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './header.css';
import {Form,Button} from 'antd';
import "antd/dist/antd.css";
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import WOW from "wowjs";
const wow = new WOW.WOW();

class Header extends React.Component {

  state={
    Info:[],
    adds:[],
  }
     constructor(props){    
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      wow.init();
      this.getInfo();
      this.getAds()
    }
    getInfo = () =>{
      axios.get(`${BASE_END_POINT}/logo`)
      .then(resposne=>{
        console.log(resposne.data.data[0])
        this.setState({Info:resposne.data.data[0]})
      })
    }
    getAds = () =>{
      axios.get(`${BASE_END_POINT}ads?limit=10`)
      .then(response=>{
          console.log('ADS  ',response.data.data)
          this.setState({adds:response.data.data}) 
      })
      .catch(error=>{
        console.log('ADS ERROR  ',error.response)
      })
  }

    
  
  
    render() {
        const {currentUser} = this.props
        var info = this.state.Info
        let {adds} = this.state
        console.log(info);
        console.log(info.description);
        const {isRTL} = this.props;
      return ( 
        <div>
          <div className='header'>
             <Nav></Nav>
              <div className='bottomHeader'>
                  
                <MDBContainer> 
                    <MDBRow> 
                        <MDBCol md="4" className='left'>
                            <h1 className='animated fadeInLeft delay-0.5s'> {isRTL?this.state.Info.arabicDescription:this.state.Info.description}</h1>
                            {!currentUser&&
                            <NavLink to='/Login'>
                            <Button className='loginButton '> {allStrings.login} </Button>
                            </NavLink>
                            }

                            {!currentUser&&
                            <NavLink to='/SignUp'>
                            <Button className='signupButton'> {allStrings.SignUp} </Button>
                            </NavLink>
                            }
                            <div style={{margin:'30px'}}>
                            <img width="250" src={info.img}></img>
                            </div>
                        </MDBCol>
                        
                        <MDBCol md="8" className='right'>
                          <div style={{height:'400px',marginTop:'10px',marginBottom:'50px'}}>
                          <div className='asdSlider '>
                            <Carousel
                            autoPlay
                            autoPlaySpeed={1500}
                            additionalTransfrom={0}
                            arrows
                            autoPlaySpeed={3000}
                            centerMode={false}
                            className=""
                            containerClass="container"
                            dotListClass=""
                            draggable
                            focusOnSelect={false}
                            infinite
                            itemClass=""
                            keyBoardControl
                            minimumTouchDrag={80}
                            renderButtonGroupOutside={false}
                            renderDotsOutside
                            responsive={{
                                desktop: {
                                breakpoint: {
                                    max: 3000,
                                    min: 1024
                                },
                                items: 1
                                },
                                mobile: {
                                breakpoint: {
                                    max: 464,
                                    min: 0
                                },
                                items: 1
                                },
                                tablet: {
                                breakpoint: {
                                    max: 1024,
                                    min: 464
                                },
                                items: 1
                                }
                            }}
                            showDots={false}
                            sliderClass=""
                            swipeable
                            >
                            {adds.map(ads=>(
                                <img
                                src={ads.img}
                                style={{
                                    display: 'block',
                                    height: '370px',
                                    margin: 'auto',
                                    width: '100%'
                                }}
                                />
                            ))}
                            </Carousel>
                            </div>
                          </div>
                        </MDBCol>
                       
                    </MDBRow>
                    
                </MDBContainer>
                
              </div>
              
              
          </div>
           
      </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Header = Form.create({ name: 'normal_login' })(Header));
