import React from 'react';
import {Icon} from 'react-materialize';
import './footer.css';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import WOW from "wowjs";
import {ChangeLanguage} from '../../actions/LanguageAction'
import {NavLink} from 'react-router-dom';


const wow = new WOW.WOW();
class Footer extends React.Component {


     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      wow.init();
    }

    saveLang = (lang) =>{
      if(lang=='ar'){
        this.props.ChangeLanguage(true)
        localStorage.setItem('@LANG','ar')
        console.log("arabic lang  ",lang)
        allStrings.setLanguage('ar')
      }else{
        this.props.ChangeLanguage(false)
        localStorage.setItem('@LANG','en')
        console.log("english lang  ",lang)
        allStrings.setLanguage('en')
      }
    }
    

    
  
  
    render() {
        
      return ( 
        <div className='footerStyle'>     
            <MDBContainer> 
                <MDBRow>
                    <MDBCol md="3">
                    <img  src='https://res.cloudinary.com/ishabrawy/image/upload/v1576193810/goiogvmbddrapxonqvj5.png'></img>
                    <br></br>
                    <br></br>
                    <ul className='wow slideInUp'>          
                        <li>{allStrings.callUs}</li>     
                        <li>+2001066808844</li>   
                        <li>souqelteb@gmail.com</li>
                        <li>Infront of technology school,Ismailia</li>
                    </ul>
                    </MDBCol>
                    <MDBCol md="3" style={{ paddingTop: '17px'}}>
                    <br></br>
                    <br></br>
                        <ul className='wow slideInUp'>
                            <li><NavLink to='/AboutUs'>{allStrings.aboutUs}</NavLink></li>
                            <li><NavLink to='/Usage'>{allStrings.usage}</NavLink></li>
                            <li><NavLink to='/Terms'>{allStrings.terms}</NavLink></li>  
                            <li><NavLink to='/ContactUs'>{allStrings.contactUS}</NavLink></li>
                            <li style={{cursor: 'pointer'}} onClick={()=>{
                              this.saveLang('en');
                             
                              }} >English</li>
                            <li style={{cursor: 'pointer'}} onClick={()=>{
                              this.saveLang('ar');
                             
                              }} >العربيه</li>
                        </ul>
                    </MDBCol>
                    
                    <MDBCol md="3" style={{ paddingTop: '13px'}}>
                    <br></br>
                    <br></br>
                    
                        <ul className='wow slideInUp'>
                            <li>{allStrings.BuyProducts}</li>
                            <li>{allStrings.discServ}</li>
                            <li>{allStrings.OnlinePayment}</li>
                            <li>{allStrings.CashPayment}</li>
                            <li>{allStrings.ShippingProducts}</li>
                        </ul> 
                    </MDBCol>
                    <MDBCol md="3">
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                        <ul>
                            <li style={{display:'inline',margin: '9px',cursor: 'pointer'}}><img src='https://res.cloudinary.com/ishabrawy/image/upload/v1576311856/mchfydkcea7phomggezz.png'></img></li>
                            <li style={{display:'inline',margin: '9px',cursor: 'pointer'}}><img src='https://res.cloudinary.com/ishabrawy/image/upload/v1576311824/u2xiwwe11rgldwfghnhx.png'></img></li>
                            <li style={{display:'inline',margin: '9px',cursor: 'pointer'}}><img src='https://res.cloudinary.com/ishabrawy/image/upload/v1576311802/q37vg5gzuwf3ebyoalsz.png'></img></li>
                            <li style={{display:'inline',margin: '9px',cursor: 'pointer'}}><img src='https://res.cloudinary.com/ishabrawy/image/upload/v1576311760/gkglbsxez0rv8tz74cje.png'></img></li>
                        </ul>
                    </MDBCol>
                </MDBRow>
                
            </MDBContainer>
        </div>
           
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    ChangeLanguage,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Footer);
