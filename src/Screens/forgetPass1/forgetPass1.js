import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './forgetPass1.css';
import {message,Form, Input, Button} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer, MDBRow} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';

class ForgetPass1 extends React.Component {


      state={
        page:1,
        email:''
      }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

         
    sendEmail = (e) => {
      e.preventDefault();
    var msg = message.loading("ًWAIT")
      this.props.form.validateFieldsAndScroll(['email'],(err, values) => {
        if (!err) {
        console.log('Received values of form: ', values);
          const data = {
            email: values.email
          }
          axios.post(`${BASE_END_POINT}sendCode`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then(response=>{
            console.log("done   ")
            this.setState({page:2,email:values.email})
            this.props.form.resetFields()
            msg.then(() => message.success('DONE', 2.5))
          })
          .catch(error=>{
            console.log("error  ", error)
            msg.then(() => message.error('ERROR', 2.5))
          })
        }
      })    
    }

    sendCode = (e) => {
      e.preventDefault();
      var msg = message.loading("ًWAIT")
      this.props.form.validateFieldsAndScroll(['confirmCode'],(err, values) => {
        if (!err) {
        console.log('Received values of form: ', values);
       
          const data = {
            email: this.state.email,
            verifycode:values.confirmCode
          }
          axios.post(`${BASE_END_POINT}confirm-code`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then(response=>{
            console.log("done   ")
            this.setState({page:3})
            this.props.form.resetFields()
            msg.then(() => message.success('DONE', 2.5))
          })
          .catch(error=>{
            console.log("error  ", error)
            msg.then(() => message.error('ERROR', 2.5))
          })
        }
      })    
    }


    sendNewPassword = (e) => {
      e.preventDefault();
      var msg = message.loading("ًWAIT")
      this.props.form.validateFieldsAndScroll(['password','confirmPassword'],(err, values) => {
        if (!err) {
        console.log('Received values of form: ', values);
        
          const data = {
            email: this.state.email,
            newPassword:values.password,
          }
          if(values.password == values.confirmPassword){
          axios.post(`${BASE_END_POINT}reset-password`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then(response=>{
            console.log("done   ")
            this.props.history.push('/Login')
            this.props.form.resetFields()
            msg.then(() => message.success('DONE', 2.5))
              //l.then(() => message.success(allStrings.updatedDone, 2.5))
              //this.props.history.push('/countries') 
          })
          .catch(error=>{
            console.log("error  ", error)
            msg.then(() => message.error('ERROR', 2.5))
          })
        }else{
          msg.then(() => message.error('Password must be same the confirm password', 2.5))
        }

        }
      })    
    }
    

    
  
  
    render() {
      const { getFieldDecorator } = this.props.form;
      const {page,email} = this.state
      const {isRTL} = this.props
          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            {page==1&&
            <MDBRow>
              <div className='login'>
                <h1>{allStrings.recoveryAccount}</h1>
                <p>{allStrings.step1Desc}</p>
                <Form onSubmit={this.sendEmail} className="login-form">                                      
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Please enter email' },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>
                </Form>
                <NavLink to='/ForgetPasswordStep2'>
                   <Button className='nextBtn' onClick={this.sendEmail}>{allStrings.next}</Button>
                </NavLink>
              </div>
            </MDBRow>
            }

            {page==2&&
            <MDBRow>
            <div className='login'>
              <h1>{allStrings.recoveryAccount}</h1>
              <p>{allStrings.step2Desc}</p>
              <div className='theemail'>
                <p> <i class="large material-icons eIcon">account_circle</i>{email}</p>
              </div>
              <p style={{marginBottom: '5px',fontSize: '13px'}}>{allStrings.step2Desc2}</p>
              <p style={{fontSize: '13px'}}>{email}</p>
              <Form onSubmit={this.sendCode} className="login-form">                                      
                  <Form.Item>
                  {getFieldDecorator('confirmCode', {
                      rules: [
                        { required: true, message: 'Please enter confirm code' },
                      ],
                  })(
                      <Input placeholder={allStrings.confirmeCode} />
                  )}
                  </Form.Item>
              </Form>
              <NavLink to='/ForgetPasswordStep3'>
                 <Button className='nextBtn' onClick={this.sendCode}>{allStrings.next}</Button>
              </NavLink>
            </div>
          </MDBRow>
            }

            {page==3&&
            <MDBRow>
            <div className='login'>
              <h1>{allStrings.recoveryAccount}</h1>
              <p>That help show that this account really belong to you</p>
              <div className='theemail'>
                <p> <i class="large material-icons eIcon">account_circle</i>{email}</p>
              </div>
              <p style={{fontSize: '13px'}}>{allStrings.EnterNewPass}</p>
              <Form onSubmit={this.sendNewPassword} className="login-form">                                      
                <Form.Item>
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please enter  Password' }],
                })(
                    <Input.Password placeholder={allStrings.newPassword} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('confirmPassword', {
                    rules: [{ required: true, message: 'Please enter  Password' }],
                })(
                    <Input.Password placeholder={allStrings.confirmPassword} />
                )}
                </Form.Item> 
              </Form>
              <NavLink to='/ForgetPasswordStep3'>
                 <Button className='nextBtn' onClick={this.sendNewPassword}>{allStrings.next}</Button>
              </NavLink>
            </div>
          </MDBRow>
            }

            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(ForgetPass1 = Form.create({ name: 'normal_login' })(ForgetPass1));
