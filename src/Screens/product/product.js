import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './product.css';
import { message,Modal, Form, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {NavLink} from 'react-router-dom';
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import moment from 'moment'
import ProductCard from '../../components/product card/product card'
import {addProductToBasket,removeProductFromBasket} from '../../actions/BasketAction'
import {getUser} from '../../actions/AuthActions'
import LoadCard from '../../components/load card/load card'
import Skeleton from 'react-loading-skeleton';
import LoadingBar from 'react-redux-loading-bar'

class Product extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
    page=1
    state={
      product:null,
      productImages:[],
      loading:true,
      comments:[],
      commentsLoading:true,
      pages:1,
      relatedProducts:[],
      relatedProductsLoading:true,
      doRate:3,
      seeMoreAllDescription:false
    }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      console.log("user   ",this.props.currentUser)
      console.log("id   ",this.props.match.params.id)
      this.getProductByID(this.props.match.params.id)
    }
    addProductToDataBase = (id) => {      
      axios.post(`${BASE_END_POINT}cart/${id}/products`, {}, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      }).then(response=>{
         console.log('addProductToApi Done');
         this.props.addProductToBasket({...response.data.product,count:1,selectedColor:'',selectedSize:''})
      }).catch(error => {
          console.log('addProductToApi  ',error.status);
          console.log('addProductToApi  ',error.respone);
          console.log('addProductToApi  ',error);
        });
  }

    getProductByID = (id) =>{
      this.page=1
      this.setState({comments:[],relatedProducts:[]})
      axios.get(`${BASE_END_POINT}products/${id}`)
      .then(response=>{
        const product = response.data
        console.log('data  ',product)
        this.setState({product:product,loading:false})
        this.getComments(id)
        this.getRlatedProducts(product.category.id)
        product.img.map(i=>{
          this.setState({
            productImages:
            [...this.state.productImages,
              {
                original: i,
                thumbnail: i,
              }   
            ]
          })
        })
      })
      .catch(error=>{
        //this.setState({loading:false})
      })
    }

    getRlatedProducts = (catID) =>{
      axios.get(`${BASE_END_POINT}products?category=${catID}&limit=4`)
      .then(response=>{
        console.log('getRlatedProducts  ',response.data)
        this.setState({relatedProducts:response.data.data,relatedProductsLoading:false})
      })
      .catch(error=>{
        console.log('getRlatedProducts error  ')
        //this.setState({relatedProductsLoading:false})
      })
    }


    getComments = (id,page,reload) =>{
      axios.get(`${BASE_END_POINT}products/${id}/findProductRate?page=${page}&limit=20`)
      .then(response=>{
        console.log('data  ',response.data)
        this.setState({pages:response.data.pageCount, comments:reload?response.data.data:[...this.state.comments,...response.data.data],commentsLoading:false})
      })
      .catch(error=>{
        //this.setState({commentsLoading:false})
      })
    }

    
  fav = (id) => {      
    axios.post(`${BASE_END_POINT}favourite/${id}/products`, {}, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('Fav Done');
       //this.setState({favDone:true })
       const user = {...this.props.currentUser,user:{...response.data.user}}
       console.log('current user')
       console.log(user)
       this.props.getUser(user)
       localStorage.setItem('@USER', JSON.stringify(user));
    }).catch(error => {
        
        console.log('Fav error  ',error.status);
        console.log('Fav error  ',error.respone);
        console.log('Fav error  ',error);
        
      });
  }

  removeFav = (id) => {
        
    axios.delete(`${BASE_END_POINT}favourite/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    }).then(response=>{
       console.log('un Fav Done');
       //this.setState({favDone:false })
       const user = {...this.props.currentUser,user:{...response.data.user}}
       this.props.getUser(user)
       localStorage.setItem('@QsathaUser', JSON.stringify(user));
  
    }).catch(error => {

        console.log('Fav error  ',error.status);
        console.log('Fav error  ',error.respone);
        console.log('Fav error  ',error);
      });
  }
  

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
         console.log('RATE AND COMMENT   ',values.comment,"  ",this.state.doRate)
          const comment = {
            rate:this.state.doRate,
            comment:values.comment
          }
          axios.put(`${BASE_END_POINT}products/${this.props.match.params.id}/rate`,JSON.stringify(comment),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            const commentInof= {
              user:this.props.currentUser.user,
              rate:this.state.doRate,
              comment:values.comment,
            }
            this.setState({comments:[commentInof,...this.state.comments]})
            console.log("ADD COMMENT DONE  ",response.data)
            this.props.form.resetFields()
            this.setState({doRate:3})
            //this.page=1
            //this.getComments(this.props.match.params.id,1,true)

          })
          .catch(error=>{
            console.log("ADD COMMENT ERROR  ")
            console.log(error)
            console.log(error.response)
          })
        }
      });
    };
    
  
    

    render() {
      const { getFieldDecorator } = this.props.form;
      const {currentUser} = this.props
      const {seeMoreAllDescription,relatedProductsLoading, loading,product,comments,commentsLoading, relatedProducts,doRate,productImages} = this.state

     
        
          
      return ( 
        <div> 
         
          <Nav></Nav>
            
            <MDBContainer> 
           
            
              <MDBRow>
              {loading?
              <MDBCol md="4">
              <Skeleton height={300} />
              </MDBCol>
              :
              <MDBCol md="4">
                  <div className='productSlider'>
                  <ImageGallery items={productImages} />
                  <div
                  onClick={()=>{
                    if(currentUser){
                      if(currentUser.user.favourite.includes(product.id)){
                          this.removeFav(product.id)
                      }else{
                          this.fav(product.id)
                      }
                    }else{
                      this.props.history.push('/Login')
                    }
                  }}
                   className='productLove'>
                        <img src={currentUser?currentUser.user.favourite.includes(product.id)?require('../../assets/images/redHeart.png'):require('../../assets/images/grayHeart.png'):require('../../assets/images/grayHeart.png')}></img>
                    </div>
                  </div>
              </MDBCol>
              }
              {loading?
              <MDBCol md="8" style={{marginTop:20}} >
              <Skeleton height={30} />
              <Skeleton height={30} />
              <Skeleton height={30} />
              <div style={{marginTop:50}}>
              <Skeleton height={30} />
              <Skeleton height={30} />
              <Skeleton height={30} />
              </div>
             
              </MDBCol>
              :
              <MDBCol md="8">
              <div className='productInfo'>
                    <p className="name">{product.name}</p>
                    <p className='price'><span>{allStrings.price}:</span> {product.hasOffer?product.offerPrice:product.price} {allStrings.EGP}</p>
                    <p className='soled'><span>{allStrings.SoledBy}: </span>{product.company}</p>
                    
                    <Rater rating={product.rate} total={5} interactive={false} />
                    <span> (<span className='totalRate'>{product.rateNumbers}</span>)</span>
                    <br></br>
                    <br></br>
                    <p  style={{color:product.available?'#2a2e43':'red',fontWeight: '600'}}>{product.available?allStrings.available:allStrings.unavailable}</p>
                    {product.hasColors &&
                    <div style={{minHeight:'30px'}}>
                      <span className='price'>{allStrings.colors} </span>
                      <div style={{paddingTop: '5px'}}>
                      {product.color.map(
                          val=>
                          <span style={{fontWeight:400,margin:'2px',width:'20px',height:'20px',borderRadius:'50%',display: 'inline-block',background:val}} value={val}></span>
                      )}
                      </div>
                      
                    </div>
                    }
                    {product.hasSizes &&
                    <div style={{minHeight:'30px'}}>
                      <span className='price'>{allStrings.sizes} : </span>
                      {product.sizes.map(
                          val=><span style={{fontWeight:400,margin:'2px'}} value={val}>{val}.</span>
                      )}
                    </div>
                    }

                    <p className="name">{allStrings.description}</p>

                    <p className='desc'>
                     {seeMoreAllDescription?product.description:product.description.substring(50)+"..."}
                    </p>
                    
                     {seeMoreAllDescription?
                     <a onClick={()=>{this.setState({seeMoreAllDescription:false})}} className='moredesc'>{allStrings.seeLess}</a>
                     :
                     <a onClick={()=>{this.setState({seeMoreAllDescription:true})}} className='moredesc'>{allStrings.seeMore}</a>
                     }
                    <br></br>
 
              </div>
              </MDBCol>
              }
             </MDBRow>
             
              {!loading&&
              <MDBRow>
                 {product.available ==true &&
              <Button className='addBtn' onClick={()=>{
                if(this.props.currentUser){
                this.props.addProductToBasket({...product,count:1})
                this.addProductToDataBase(product.id)
                message.success(allStrings.addproductToBasket);
              }else{
                this.props.history.push('/Login')
              }
              }}>{allStrings.addToCart}</Button>
              }
               {product.available ==false &&
              <Button disabled className='addBtn' onClick={()=>{
                if(this.props.currentUser){
                this.props.addProductToBasket({...product,count:1})
                message.success(allStrings.addproductToBasket);
              }else{
                this.props.history.push('/Login')
              }
              }}>{allStrings.addToCart}</Button>
              }
              </MDBRow>
              }
              
              <MDBRow>
              <MDBCol>
                 
                
                  <div className='comments' >
                    {commentsLoading?
                    <div style={{marginTop:30}} >
                        <Skeleton height={30}/>
                        <Skeleton height={30}/>
                    </div>
                    :
                    <div>
                    <p className='name' style={{fontSize: '22px'}}>{allStrings.ProductReview}</p>
                    <Form onSubmit={this.handleSubmit} className="login-form">                                      
                        <Form.Item>
                        {getFieldDecorator('comment', {
                            rules: [
                              { required: true, message: 'Please write a reveiw' },
                            ],
                        })(
                            <Input 
                            onPressEnter={(e)=>{
                               this.handleSubmit(e)
                            }} placeholder={allStrings.comment}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('rate', {
                            rules: [{ required: false, message: 'Please enter  rate' }],
                        })(
                            <Rater onRate={(val)=>{
                              console.log("RATING  ",val.rating)
                              this.setState({doRate:val.rating})
                            }} total={5} rating={doRate} />
                        )}
                        </Form.Item>
                    </Form>

                    

                    </div>
                    }

                    {commentsLoading?
                    <div style={{marginTop:60}} >
                      <div style={{display:'flex',flexDirection:'row',alignItems:'center' }}>
                      <Skeleton circle={true} height={70} width={70} />
                       <div style={{marginLeft:20}} >
                       <Skeleton height={30} width={300} />
                       </div>
                      </div>
                      <Skeleton height={30} />
                      <Skeleton height={30} />
                    </div>
                    :
                    comments.length>0 &&
                    <div className='allComments'>
                     {comments.length>0?
                     comments.map(comment=>(
                    <div className='comment'>
                       <div className='commentTop'>
                           <img src={comment.user.img.length>0?comment.user.img:require('../../assets/images/soeqprofileicon.jpg')}></img>
                           <span className='nameSpan'>
                             {comment.user.firstname+" "+comment.user.lastname}
                             <span className='city'>{comment.user.address}</span>
                           </span>
                           <span className='commentTime'>{moment(comment.createdAt).fromNow()}</span>
                           <Rater rating={comment.rate} total={5} interactive={false} />
                           
                           <br></br>
                         
                       </div>
                       <div className='commentLow'>
                             <p className='desc' style={{paddingLeft:'8%',paddingRight:'8%',color:'#000'}}>
                              {comment.comment}
                             </p>
                       </div>
                   </div>
                     ))
                     :
                    <div style={{marginBottom:20, textAlign:'center'}} >{allStrings.noComments} </div>
                     }
                      
                    </div>
                    }
                  </div>
                  
                   {
                   comments.length>0&&
                    <MDBRow>
                       <Button className='addBtn' onClick={()=>{
                         if(this.state.pages>this.page){
                          console.log('do action')
                           this.getComments(this.page+1)
                           this.page = this.page+1
                         }else{
                           console.log('stop action  ',this.state.pages, "  ",this.page)
                         }
                       }}>{allStrings.seeMore}</Button>
                    </MDBRow>
                    
                   }
                    <MDBRow>
                       <div style={{textAlign:'center', width:'100%'}}>
                         <p style={{fontWeight: '500',margin: '40px 0px'}}>{allStrings.alsoLike}</p>
                       </div>
                       
                       {relatedProductsLoading?
                         [1,2,3,4].map(product=>(
                          <MDBCol md="6" lg="3" sm="6" style={{marginBottom: '30px'}}>
                           <LoadCard/>
                         </MDBCol>
                         ))
                        :
                       relatedProducts.map(product=>(
                         <MDBCol md="6" lg="3" sm="6" style={{marginBottom: '30px'}}>
                          <ProductCard go data={product} history={this.props.history} />
                        </MDBCol>
                       ))}
                       
                      
                       
                    </MDBRow>

                  </MDBCol>
              </MDBRow>
            </MDBContainer>
            
            <Footer></Footer>
        </div>
      );
    }
  }
  

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    addProductToBasket,
    getUser
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Product = Form.create({ name: 'normal_login' })(Product));
