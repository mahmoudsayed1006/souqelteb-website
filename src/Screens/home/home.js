import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './home.css';
import { Skeleton, message,Form, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Header from '../header/header';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import {getUser} from '../../actions/AuthActions'
import {ChangeLanguage} from '../../actions/LanguageAction'
import {getNotifications,getUnreadNotificationsCount} from '../../actions/NotificationAction'
import ProductCard from '../../components/product card/product card'
import LoadCard from '../../components/load card/load card'
import WOW from "wowjs";

const wow = new WOW.WOW();

class Home extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0)
      }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      wow.init();
      this.checkLogin()
      this.checkLanguage()
      this.getCategories()
      this.getOffers()
      this.getAds()
      this.getTopProducts()
    }
   
    state = {
        rating: 1,
        categories:[],
        categoryLoading:true,
        offers:[],
        offersLoading:true,
        adds:[],
        topProducts: [],
        topProductLoading:true,
        category1:null,
        productsOfCategory1:[],
        category2:null,
        productsOfCategory2:[],
        productsOfCategoryLoad:true,

      };

      checkLogin = ()=>{
          const user = localStorage.getItem('@USER')
          if(user){
              const u = JSON.parse(user)
              console.log("EXIST USER   ",u)
              this.props.getUser(u)
              this.props.getNotifications(1,this.props.currentUser.token)
              this.props.getUnreadNotificationsCount(this.props.currentUser.token)
          }else{
            console.log("NOT EXIST USER")
          }
      }

      checkLanguage = ()=>{
        const lang = localStorage.getItem('@LANG')
            if(lang=='ar'){
                this.props.ChangeLanguage(true)
                localStorage.setItem('@LANG','ar')
                console.log("arabic lang  ",lang)
            }else{
                this.props.ChangeLanguage(false)
                localStorage.setItem('@LANG','en')
                console.log("english lang  ")
            }
    }

      getCategories = () =>{
          axios.get(`${BASE_END_POINT}categories`)
          .then(response=>{
              console.log('CATEGORIES  ',response.data.data)
              const cat = response.data.data;
              this.setState({categoryLoading:false, categories:cat}) 
              if(cat.length>0){

                  if(cat[cat.length-1]){
                      console.log('cat[0] exist   ')
                      this.setState({category1:cat[cat.length-1]})
                      this.getCategoryProduct(cat[cat.length-1].id,0)
                  }

                  if(cat[cat.length-2]){
                    console.log('cat[1] exist')
                    this.setState({category2:cat[cat.length-2]})
                    this.getCategoryProduct(cat[cat.length-2].id,1)
                }
              }
          })
          .catch(error=>{
            console.log('CATEGORIES ERROR  ',error.response)
          })
      }

      getCategoryProduct = (id,index) => {
          //&available=true
        axios.get(`${BASE_END_POINT}products?category=${id}&limit=20`)
        .then(response=>{
            console.log('products of CATEGORIES  ',index,"   ",response.data.data)
            if(index==0){
                this.setState({productsOfCategory1:response.data.data})
            } 

            if(index==1){
                this.setState({productsOfCategory2:response.data.data})
            } 
        })
        .catch(error=>{
          console.log('CATEGORIES products ERROR  ',error.response)
        })
      }
    

    getOffers = () =>{
        //&available=true
        axios.get(`${BASE_END_POINT}products/?hasOffer=true&limit=20`)
        .then(response=>{
            console.log('OFFERS  ',response.data.data)
            this.setState({offers:response.data.data,offersLoading:false}) 
        })
        .catch(error=>{
         //this.setState({offersLoading:false})
          console.log('OFFERS ERROR  ',error.response)
        })
    }

    getAds = () =>{
        axios.get(`${BASE_END_POINT}ads?limit=10`)
        .then(response=>{
            console.log('ADS  ',response.data.data)
            this.setState({adds:response.data.data}) 
        })
        .catch(error=>{
          console.log('ADS ERROR  ',error.response)
        })
    }

    getTopProducts = () =>{
        axios.get(`${BASE_END_POINT}products/?top=true&limit=20`)
        .then(response=>{
            console.log('TOP PRODUCTS  ',response.data.data)
            this.setState({topProductLoading:false, topProducts:response.data.data}) 
        })
        .catch(error=>{
            //this.setState({topProductLoading:false,})
          console.log('TOP PRODUCTS ERROR  ',error.response)
        })
    }

    productLoadingView = () =>(
        <div className='slideItem'>
            <NavLink to='/'>
                <img style={{width:'100%',height:'200px',margin:'0px'}} src={require('../../assets/images/loadImage.png')}></img>
            </NavLink>
            <div className='cart'>
                <img src="https://res.cloudinary.com/ishabrawy/image/upload/v1576352911/kicmx67qi460kz4inb3l.png"></img>
            </div>
            <div className='love'>
                <img src="https://res.cloudinary.com/ishabrawy/image/upload/v1576352934/ygjelfj2oyb1kq3hgfqw.png"></img>
            </div>
            <div className='bottom'>
                <p className='pname'></p>
                <Rater rating={0} total={5} interactive={false} />
                <p style={{color:'#000',fontWeight: '400'}}></p>
            </div>
            
        </div>
    )

    
  
  
    render() {
        const {category1,category2,productsOfCategory1,productsOfCategory2,  rating, categories,categoryLoading, offers, offersLoading,topProductLoading, adds, topProducts } = this.state;
        const {isRTL} = this.props;

          const CustomButtonGroupAsArrows = ({ next, previous }) => {
            return (
              <div className='arrowCostom'
                style={{
                  textAlign: "center",
                  float: 'right',
                  marginRight: '0%',
                  marginTop: '-488px'
                }}
              >
                <button className='arrow' style={{background: 'transparent',border: '0px',marginRight:'15px'}} onClick={previous}>
                    <Icon>arrow_back</Icon>
                </button>
                <button className='arrow' style={{background: 'transparent',border: '0px'}} onClick={next}>
                    <Icon>arrow_forward</Icon>
                </button>
              </div>
            );
          };
          
      return ( 
        <div> 
            <Header></Header>

            <MDBContainer> 
            <MDBRow>
                <div className='toBottom animated zoomIn delay-0.5s'>
                <Icon>expand_more</Icon>
                </div>
            </MDBRow>

           {/*} <div className='asdSlider wow fadeIn'>
            <Carousel
            autoPlay
            autoPlaySpeed={1500}
            additionalTransfrom={0}
            arrows
            autoPlaySpeed={3000}
            centerMode={false}
            className=""
            containerClass="container"
            dotListClass=""
            draggable
            focusOnSelect={false}
            infinite
            itemClass=""
            keyBoardControl
            minimumTouchDrag={80}
            renderButtonGroupOutside={false}
            renderDotsOutside
            responsive={{
                desktop: {
                breakpoint: {
                    max: 3000,
                    min: 1024
                },
                items: 1
                },
                mobile: {
                breakpoint: {
                    max: 464,
                    min: 0
                },
                items: 1
                },
                tablet: {
                breakpoint: {
                    max: 1024,
                    min: 464
                },
                items: 1
                }
            }}
            showDots={false}
            sliderClass=""
            swipeable
            >
            {adds.map(ads=>(
                <img
                src={ads.img}
                style={{
                    display: 'block',
                    height: '300px',
                    margin: 'auto',
                    width: '100%'
                }}
                />
            ))}
            </Carousel>
            </div>
            */}
           
            <div className='slideStyle'>
                <h1  className='title' onClick={()=>{
                this.props.history.push('/Category',{data:null,from:'Home',hasOffer:true,name2:allStrings.offers})
                }}
                >{allStrings.offers}</h1>            
                <Carousel 
                    additionalTransfrom={0}
                    arrows={false}
                    autoPlaySpeed={3000}
                    centerMode
                    className=""
                    containerClass="container"
                    dotListClass=""
                    draggable
                    focusOnSelect={false}
                    infinite
                    itemClass=""
                    keyBoardControl
                    minimumTouchDrag={80}
                    customButtonGroup={<CustomButtonGroupAsArrows />}
                    renderButtonGroupOutside={true}
                    renderDotsOutside={true}
                    responsive={{
                    desktop: {
                        breakpoint: {
                        max: 3000,
                        min: 1024
                        },
                        items: 3,
                        partialVisibilityGutter: 40
                    },
                    mobile: {
                        breakpoint: {
                        max: 464,
                        min: 0
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    },
                    tablet: {
                        breakpoint: {
                        max: 1024,
                        min: 464
                        },
                        items: 2,
                        partialVisibilityGutter: 30
                    }
                    }}
                    showDots={true}
                    sliderClass=""
                    slidesToSlide={1}
                    swipeable
                >
                {
                offersLoading?
                [1,2,3,4].map(v=><LoadCard/>)
                :
                offers.map(offer=>(
                 <ProductCard history={this.props.history} data={offer} />
                
                ))}
    
            </Carousel>
            <p 
             onClick={()=>{
                this.props.history.push('/Category',{data:null,from:'Home',hasOffer:true,name2:allStrings.offers})
            }}
             className='seeMoreBtn'>
                <svg>
                    <rect x="0" y="0" fill="none" width="166" height="45" />
                </svg>
               {allStrings.seeMore}
            </p>
          </div>

          
          <div className='slideStyle'>
                <h1 className='title' onClick={()=>{
                    this.props.history.push('/Category',{data:null,from:'Home',hasOffer:false,name2:allStrings.topProducts})
                }} 
                >{allStrings.topProducts}</h1>            
                <Carousel 
                    additionalTransfrom={0}
                    arrows={false}
                    autoPlaySpeed={3000}
                    centerMode
                    className=""
                    containerClass="container"
                    dotListClass=""
                    draggable
                    focusOnSelect={false}
                    infinite
                    itemClass=""
                    keyBoardControl
                    minimumTouchDrag={80}
                    customButtonGroup={<CustomButtonGroupAsArrows />}
                    renderButtonGroupOutside={true}
                    renderDotsOutside={true}
                    responsive={{
                    desktop: {
                        breakpoint: {
                        max: 3000,
                        min: 1024
                        },
                        items: 3,
                        partialVisibilityGutter: 40
                    },
                    mobile: {
                        breakpoint: {
                        max: 464,
                        min: 0
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    },
                    tablet: {
                        breakpoint: {
                        max: 1024,
                        min: 464
                        },
                        items: 2,
                        partialVisibilityGutter: 30
                    }
                    }}
                    showDots={true}
                    sliderClass=""
                    slidesToSlide={1}
                    swipeable
                >
              {
                topProductLoading?
                [1,2,3,4].map(v=><LoadCard/>)
                :
                topProducts.map(topProduct=>(
                <ProductCard history={this.props.history} data={topProduct} />
                ))
              }
            </Carousel>
            <p  onClick={()=>{
                this.props.history.push('/Category',{data:null,from:'Home',hasOffer:false,name2:allStrings.topProducts})
            }} className='seeMoreBtn'>
                <svg>
                    <rect x="0" y="0" fill="none" width="166" height="45" />
                </svg>
                {allStrings.seeMore}
            </p>
          </div>
          
          {productsOfCategory1.length>0&&
          <div className='slideStyle'>
                <h1 className='title'
                    onClick={()=>{
                    this.props.history.push('/Category',{data:categories[categories.length-1],from:'Home',hasOffer:false,name2:isRTL?category1.arabicname:category1.categoryname})
                }} >
                    {isRTL?category1.arabicname:category1.categoryname}</h1>            
                <Carousel
                    additionalTransfrom={0}
                    arrows={false}
                    autoPlaySpeed={3000}
                    centerMode
                    className=""
                    containerClass="container"
                    dotListClass=""
                    draggable
                    focusOnSelect={false}
                    infinite
                    itemClass=""
                    keyBoardControl
                    minimumTouchDrag={80}
                    customButtonGroup={<CustomButtonGroupAsArrows />}
                    renderButtonGroupOutside={true}
                    renderDotsOutside={true}
                    responsive={{
                    desktop: {
                        breakpoint: {
                        max: 3000,
                        min: 1024
                        },
                        items: 3,
                        partialVisibilityGutter: 40
                    },
                    mobile: {
                        breakpoint: {
                        max: 464,
                        min: 0
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    },
                    tablet: {
                        breakpoint: {
                        max: 1024,
                        min: 464
                        },
                        items: 2,
                        partialVisibilityGutter: 30
                    }
                    }}
                    showDots={true}
                    sliderClass=""
                    slidesToSlide={1}
                    swipeable
                >
             {
             productsOfCategory1.map(product=>(
            <ProductCard history={this.props.history} data={product} />
            ))
            }
            
            </Carousel>
            <p  onClick={()=>{
                this.props.history.push('/Category',{data:categories[categories.length-1],from:'Home',hasOffer:false,})
            }} className='seeMoreBtn'>
                <svg>
                    <rect x="0" y="0" fill="none" width="166" height="45" />
                </svg>
               {allStrings.seeMore}
            </p>
          </div>
            }


        {productsOfCategory2.length>0&&
          <div className='slideStyle'>
                <h1 className='title' onClick={()=>{
                    this.props.history.push('/Category',{data:categories[categories.length-2],from:'Home',hasOffer:false,})
                }}
                >{isRTL?category2.arabicname:category2.categoryname}</h1>            
                <Carousel
                    additionalTransfrom={0}
                    arrows={false}
                    autoPlaySpeed={3000}
                    centerMode
                    className=""
                    containerClass="container"
                    dotListClass=""
                    draggable
                    focusOnSelect={false}
                    infinite
                    itemClass=""
                    keyBoardControl
                    minimumTouchDrag={80}
                    customButtonGroup={<CustomButtonGroupAsArrows />}
                    renderButtonGroupOutside={true}
                    renderDotsOutside={true}
                    responsive={{
                    desktop: {
                        breakpoint: {
                        max: 3000,
                        min: 1024
                        },
                        items: 3,
                        partialVisibilityGutter: 40
                    },
                    mobile: {
                        breakpoint: {
                        max: 464,
                        min: 0
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    },
                    tablet: {
                        breakpoint: {
                        max: 1024,
                        min: 464
                        },
                        items: 2,
                        partialVisibilityGutter: 30
                    }
                    }}
                    showDots={true}
                    sliderClass=""
                    slidesToSlide={1}
                    swipeable
                >
             {
             productsOfCategory2.map(product=>(
            <ProductCard history={this.props.history} data={product} />
            ))
            }
            
            </Carousel>
            <p onClick={()=>{
                this.props.history.push('/Category',{data:categories[categories.length-2],from:'Home',hasOffer:false,})
            }} className='seeMoreBtn'>
                <svg>
                    <rect x="0" y="0" fill="none" width="166" height="45" />
                </svg>
               {allStrings.seeMore}
            </p>
          </div>
            }



            <div className='categoties'>     
            <MDBContainer> 
                <h1 className='title'>{allStrings.categories}</h1>
                <MDBRow>
                    {
                    categoryLoading?
                    <LoadCard/>
                    :
                    categories.map(category=>(
                    <MDBCol md="2" sm="4" lg="2" xm="6" className='categoryIcon'>
                        <div className='wow bounceIn ' onClick={()=>{
                            this.props.history.push('/Category',{data:category,from:'Home',hasOffer:false})
                        }}>
                        <img style={{width:'100%',height:'100px'}} src={category.img}></img>
                        <p style={{fontWeight:' 400',textAlign: 'center'}}>{isRTL?category.arabicname:category.categoryname}</p>
                        </div>
                    </MDBCol>
                    ))}
                </MDBRow>
                
            </MDBContainer>
        </div>
           
                    
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    getUser,
    ChangeLanguage,
    getNotifications,
    getUnreadNotificationsCount
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Home = Form.create({ name: 'normal_login' })(Home));
