import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './about.css';
import {message,Form, Input, Button} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer, MDBRow} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';

class About extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

      state={
        page:1,
        about:''
      }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }

      this.getAbout()
    }

    getAbout = () =>{
      axios.get(`${BASE_END_POINT}/about`)
      .then(resposne=>{
        this.setState({about:resposne.data[0].about})
      })
    }

  
    render() {
      const { getFieldDecorator } = this.props.form;
      const {page,email,about} = this.state
      const {isRTL} = this.props
          
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              <div className="aboutUs">
                <br></br>
                <h1>{allStrings.aboutUs} : </h1>
                <div className="aboutBody">
                  <p>
                    {about}
                  </p>
                </div>
              </div>
            </MDBRow>
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(About = Form.create({ name: 'normal_login' })(About));
