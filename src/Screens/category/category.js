import React from 'react';
import './category.css';
import { Button,Form,Menu,Dropdown,Icon,Pagination,Collapse,Select, Slider, Checkbox, Row, Col,Input } from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import ProductCard from '../../components/product card/product card'
import LoadCard from '../../components/load card/load card'


const { Panel } = Collapse;
const { Option } = Select;
class Category extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
     page=1;
     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      this.getCategories()
      if(this.props.location.state.from=='search'){
        this.getSearchProducts(1,this.props.location.state.name)
      }else{
        this.getCategoryProducts(1,this.props.location.state.data?this.props.location.state.data.id:null,false,false,this.props.location.state.hasOffer)
      }
      console.log("CAT ID   ",this.props.location.state.data)
    }

    state = {
      current: 1,
      expandIconPosition: 'right',
      categories:[],
      products:[],
      productsLoad:true,
      categoryId:this.props.location.state.data?this.props.location.state.data.id:null,
      from: this.props.location.state.from,
      sort:false,
      price:'',
      hasOffer:this.props.location.state.hasOffer,
      totalCount:0,
      pages:1,
      priceTo:null,
      priceFrom:null,
      owner:''

    };


    getSearchProducts = (page,name) =>{
      //&available=true
      console.log('name  ',name)
      let uri = `${BASE_END_POINT}products?name=${name}&page=${page}`
      axios.get(uri)
      .then(response=>{
          console.log('search products  ',response.data.data)
          this.setState({productsLoad:false, totalCount:response.data.totalCount,pages:response.data.pageCount, products:response.data.data}) 
      })
      .catch(error=>{
        console.log('products ERROR  ',error.response)
        console.log('products ERROR  ',error)
      })
    }


    getCategoryProducts = (page,id,refresh,sort,hasOffer,priceFrom,priceTo,owner) =>{
       //&available=true
      let uri = `${BASE_END_POINT}products?page=${page}&limit=20`
      if(id){
        uri= `${uri}&category=${id}`
        console.log("sort    ",id)
      }
      if(sort){
        uri= `${uri}&sortByPrice=true`
        console.log("sort    ",uri)
      }
      if(owner){
        uri= `${uri}&company=${owner}`
        console.log("sort    ",uri)
      }
      if(hasOffer){
        uri= `${uri}&hasOffer=true`
        console.log("offer    ",hasOffer)
      }

      if(priceTo){
        uri= `${uri}&priceTo=${priceTo}&priceFrom=${priceFrom}`
        console.log("priceTo    ",priceTo,"   ",priceFrom)
      }


      axios.get(uri)
      .then(response=>{
          console.log('products  ',response.data.data)
          this.setState({productsLoad:false, totalCount:response.data.totalCount,pages:response.data.pageCount, products:refresh?response.data.data:[...this.state.products,...response.data.data]}) 
      })
      .catch(error=>{
        console.log('products ERROR  ',error.response)
        console.log('products ERROR  ',error)
      })
    }

    getCategories = () =>{
      axios.get(`${BASE_END_POINT}categories`)
      .then(response=>{
          console.log('CATEGORIES  ',response.data.data)
          this.setState({categories:response.data.data}) 
      })
      .catch(error=>{
        console.log('CATEGORIES ERROR  ',error.response)
       
      })
  }
    onChange = page => {
      //page,id,refresh,sort,hasOffer,price
      this.getCategoryProducts(page,this.state.categoryId,true,this.state.sort,this.state.hasOffer,this.state.priceFrom,this.state.priceTo,this.state.owner)
    };
    
  
    onAfterChange = (value) => {
      console.log('onChange: ', value);
      this.setState({priceFrom:value[0],priceTo:value[1]})
      this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,value[0],value[1],this.state.owner)
    }

    render() {
      function callback(key) {
        console.log(key);
      }

      function onChangeCheck(checkedValues) {
        console.log('checked = ', checkedValues);
      }
    
      
      const price =
          <div className='priceCheck'>
             <Slider range max={100000} step={10} defaultValue={[0, 10000]} 
                onAfterChange={this.onAfterChange}
             />
                    <Checkbox.Group style={{ width: '100%' }} onChange={onChangeCheck}>
            <Row>
              <Col>
                <Checkbox onChange={(val)=>{
                  if(val.target.checked){
                    console.log("aaa  ",val.target.checked)
                    this.setState({priceFrom:0,priceTo:1000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,0,1000,this.state.owner)
                  }else{
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} value="A">0 EGY - 1000 EGY</Checkbox>
              </Col>
              <Col> 
                <Checkbox
                onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:1000,priceTo:10000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,1000,10000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} 
                 value="B">1000 EGY - 10000 EGY</Checkbox>
              </Col>
              <Col>
                <Checkbox onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:10000,priceTo:2000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,10000,20000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} value="C">10000 EGY - 20000 EGY</Checkbox>
              </Col>
              <Col>
                <Checkbox onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:20000,priceTo:25000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,20000,25000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} value="D">20000 EGY - 25000 EGY</Checkbox>
              </Col>
              <Col>
                <Checkbox onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:25000,priceTo:50000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,25000,50000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} value="E">25000 EGY - 50000 EGY</Checkbox>
              </Col>
              <Col>
                <Checkbox
                onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:50000,priceTo:70000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,50000,70000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }}
                 value="F">50000 EGY - 70000 EGY</Checkbox>
              </Col>
              <Col>
                <Checkbox onChange={(val)=>{
                  if(val.target.checked){
                    this.setState({priceFrom:70000,priceTo:100000})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,70000,100000,this.state.owner)
                  }else{
                    this.setState({priceFrom:null,priceTo:null})
                    this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,null,this.state.owner)
                  }
                }} value="G">70000 EGY - 100000 EGY</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
          </div>
      ;
      
      const { getFieldDecorator } = this.props.form;
      const menu = (
        <Menu>
          <Menu.Item onClick={()=>{
            this.setState({sort:true})
            this.getCategoryProducts(1,this.state.categoryId,true,true,this.state.hasOffer,this.state.priceFrom,this.state,this.state.priceTo,this.state.owner)
          }} key="0">
            <p>{allStrings.lowToTop}</p>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item 
          onClick={()=>{
            this.setState({sort:false})
            this.getCategoryProducts(1,this.state.categoryId,true,false,this.state.hasOffer,this.state.priceFrom,this.state.priceTo,this.state.owner)
          }}
           key="1">
          <p>{allStrings.topToLow}</p>
          </Menu.Item>
        </Menu>
      );
      const { expandIconPosition,categories,products,productsLoad } = this.state;
      const {isRTL} = this.props
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
              <MDBRow>
                <MDBCol md="3">
                  <p style={{marginLeft:'4px',marginTop:'50px',display:'inline-block',color:'#000',fontWeight:'400',fontSize:'19px'}} >
                    {allStrings.Home }
                    <span style={{paddingRight:'2px',paddingLeft:'2px'}}> > </span>
                  </p>
                  <span style={{ fontWeight: '400',color: '#707070',fontSize:'1.1rem'}}>{this.props.location.state.data?isRTL?this.props.location.state.data.arabicname:this.props.location.state.data.categoryname:this.props.location.state.name2}</span>
                <Collapse
                    defaultActiveKey={['1','2']}
                    onChange={callback}
                    expandIconPosition={expandIconPosition}
                   
                  >
                    {this.state.from!='Home'&&
                    <Panel header={ allStrings.categories} key="1">
                        {categories.map(category=>(
                        <div onClick={()=>{
                          this.setState({categoryId:category.id})
                          this.getCategoryProducts(1,category.id,true,this.state.sort,this.state.hasOffer,this.state.priceFrom,this.state.priceTo,this.state.owner)
                        }} style={{marginBottom:'10px',cursor:'pointer'}}>
                          <span style={{fontWeight: '400',color:' #8690a7',fontSize: '16px'}}>{isRTL?category.arabicname:category.categoryname}  </span> 
                          <span style={{display:'inline-block',float:'right',fontWeight: '500',color:' #000',fontSize: '15px'}}></span>
                        </div>
                        ))}
                    </Panel>
                    }
                    <Panel header={allStrings.price} key="2">
                      <div>{price}</div>
                    </Panel>
                  
                  </Collapse>
                  <br></br>
                  <span style={{paddingLeft:'5px', color: '#000',fontWeight:' 500',fontSize: '18px',display:'inline-block'}}>{allStrings.hasoffer}</span>
                  <Checkbox onChange={(val)=>{
                    console.log('HAS OFFER   ',val.target.checked)
                    this.setState({hasOffer:val.target.checked})
                    this.getCategoryProducts(1,this.state.categoryId,true,false,val.target.checked,this.state.hasOffer,this.state.priceFrom,this.state.priceTo,this.state.owner)
                  }} style={{float:'right'}}></Checkbox>
                  <br></br>
                  <hr></hr>
                  <br></br>

                   <span style={{paddingLeft:'5px', color: '#000',fontWeight:' 500',fontSize: '18px',display:'inline-block'}}>{allStrings.SoledBy}</span>
<br></br>
<br></br>

                   <Input 
                   onChange={e=>{
                     console.log('SOLED BY  ',e.target.value)
                     this.setState({owner:e.target.value})
                     this.getCategoryProducts(1,this.state.categoryId,true,this.state.sort,this.state.hasOffer,this.state.priceTo,this.state.priceFrom,e.target.value)

                   }}
                   style={{background:' #fff',border: '1px solid #aaa !important',paddingLeft: '8px'}} placeholder={allStrings.SoledBy} />
                </MDBCol>

                <MDBCol md="9">
                <MDBRow> 
                  <div className='categoryTop'>
                    <div>
                  <p className='name categoryName'>{allStrings.itemsOf} {this.props.location.state.data?isRTL?this.props.location.state.data.arabicname:this.props.location.state.data.categoryname:this.props.location.state.name2}</p>
                  <div className='categoryRight'>
                    <span className='arang' style={{ fontWeight: '400',color: '#707070',marginRight:'20px',display:'inline-block'}}>{allStrings.sortBy} </span>
                    <Dropdown overlay={menu} trigger={['click']} className='a'>
                      <Button>{this.state.sort?allStrings.lowToTop:allStrings.topToLow} <Icon type="down" /></Button>
                    </Dropdown>
                  </div>
                  </div>
                  <br></br>
                  <div>
                    <p style={{ fontWeight: '400',color: '#707070',marginRight:'50px'}}>{this.state.totalCount} <span style={{float:isRTL?'left':'none' ,paddingRight:'5px'}}>{allStrings.itemsFounded}</span></p>
                    <span className='price categoryPrice'>{allStrings.viewPerPage}</span>
                  </div>
                  </div>
                  
                </MDBRow>

                <MDBRow>
                  {productsLoad?
                  [1,2,3,4,5,6].map(v=>
                  <MDBCol style={{marginBottom:20}} md="6" lg="4" sm="6">
                    <LoadCard/>
                  </MDBCol>
                  )
                  :
                  products.length>0?
                  products.map(product=>(
                        <MDBCol md="6" lg="4" sm="6">
                        <ProductCard history={this.props.history} data={product} />
                     </MDBCol>
                  ))
                  :
                  <div style={{width:'100%',textAlign:'center',marginBottom:'50%'}} >
                    {allStrings.noProducts}
                  </div>

                }
                </MDBRow>

                 
                <MDBRow>
                    <div style={{width:'100%'}}>
                  <Pagination defaultPageSize={this.state.pages} onChange={this.onChange} total={this.state.pages} />
                  </div>
                  </MDBRow>

                </MDBCol>
              </MDBRow>
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Category = Form.create({ name: 'normal_login' })(Category));
