import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';


import './sign up.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm,Steps,Select } from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import Footer from '../footer/footer';
import {NavLink} from 'react-router-dom';
const { Step } = Steps;
const { TextArea } = Input;
componentDidMount() {
  window.scrollTo(0, 0)
}

class SignUp extends React.Component {

  state = {
    current: 0,
  };

  onChange = current => {
    console.log('onChange:', current);
    this.setState({ current });
  };


     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('en')
      }else{
        allStrings.setLanguage('ar')
      }
    }
    

    
  
    next() {
      const current = this.state.current + 1;
      this.setState({ current });
    }
  
    prev() {
      const current = this.state.current - 1;
      this.setState({ current });
    }

      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           let user = new FormData()
           if(this.state.current==1){
           console.log("FROM STEP 1   ",values)
           /*user.append('firstname')
           user.append('lastname')
           user.append('email')
           user.append('phone')
           user.append('type')
           user.append('signUpFrom','normal')
           user.append('type','CLIENT')
           user.append('token','')
           user.append('password')
           user.append('area')
           user.append('city')
           user.append('address')*/
           }

           if(this.state.current==2){
           console.log("FROM STEP 2   ",values)
           }
          }
          
        });
      };
    
    render() {
      const { getFieldDecorator } = this.props.form;
      const { current } = this.state;
      const Option = Select.Option;
    
      return ( 
        <div> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
            
              <div className='signUp login'>
              <div className='signUpTitle'>

              <span className='leftTitle'>Basic info</span>
              <span className='rightTitle'>Location info</span>
              </div>
              <div className='signUpIcon'>
                <NavLink to='/SignUp'>
                <span className='leftIcon'>1</span>
                </NavLink>
                
                <hr className='iconHr'></hr>
                <NavLink to='/SignUpStep2'>
                   <span className='rightIcon'>2</span>
                </NavLink>
                
              </div>
                
              <Form onSubmit={this.handleSubmit} className="login-form">  
                <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 2px)' ,marginRight:'12px' }}>
                {getFieldDecorator('firstname', {
                    rules: [
                      { required: true, message: 'Please enter first name' },
                    
                    ],
                })(
                    <Input placeholder={allStrings.firstname} />
                )}
                </Form.Item>
                <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(48% - 12px)' ,marginLeft:'4px' }}>
                {getFieldDecorator('lastname', {
                    rules: [
                      { required: true, message: 'Please enter last name' },
                    ],
                })(
                    <Input placeholder={allStrings.lastname} />
                )}
                </Form.Item>                                    
                <Form.Item>
                {getFieldDecorator('email', {
                    rules: [
                      { required: true, message: 'Please enter email' },
                      {type: 'email',  message: 'Please enter correct email' }
                    ],
                })(
                    <Input placeholder={allStrings.email} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please enter  Password' }],
                })(
                    <Input.Password placeholder={allStrings.password} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('confirmPassword', {
                    rules: [{ required: true, message: 'Please enter  Password' }],
                })(
                    <Input.Password placeholder={allStrings.password} />
                )}
                </Form.Item> 
                <Form.Item>
                {getFieldDecorator('phone', {
                    rules: [
                      { required: true, message: 'Please enter phone ,' },
                      //{type: 'number',  message: 'Please enter numbers' }
                    ],
                })(
                    <Input placeholder={allStrings.phone} />
                )}
                </Form.Item>

                </Form>
                <NavLink to='/SignUpstep2'>
                   <Button className='loginBtn' onClick={this.handleSubmit}>{allStrings.next}</Button>
                </NavLink>
               
                <p style={{textAlign:'center', color:'#000',marginTop:'30px'}}>
                <NavLink to='/Login'>i have an account sign in</NavLink>
                <hr></hr>
                </p>
                               
              </div>
            </MDBRow>
        
            </MDBContainer>
            <Footer></Footer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(SignUp = Form.create({ name: 'normal_login' })(SignUp));
