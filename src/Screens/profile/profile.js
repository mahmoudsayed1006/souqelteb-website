import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import './profile.css';
import {Form,Button,Modal,Input,Select,message} from 'antd';
import "antd/dist/antd.css";
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import {NavLink} from 'react-router-dom';
import Footer from '../footer/footer';
import {getUser} from '../../actions/AuthActions'
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
const { TextArea } = Input;

class Profile extends React.Component {

  favPage=1;
  ordersPage=1;

    state = {
      visible: false,
      editModel: false,
      citiies: [],
      areas: [],
      flag:2,
      favourites:[],
      favPages:1,
      orders:[],
      ordersPages:1,
      image:null,
      }

      componentDidMount() {
        window.scrollTo(0, 0)
        this.getFavourites(1)
        this.getOrders(1)
        console.log("Location   ",this.props.history)
      }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
          this.getCities()
          
        }

      step1 = () => {
        const flag = 1;
        this.setState({ flag });
      }

      getFavourites = (page) =>{
        axios.get(`${BASE_END_POINT}favourite/${this.props.currentUser.user.id}/users?page=${page}`)
        .then(resposne=>{
          console.log("FAVS   ",resposne.data)
          this.setState({favourites:[...this.state.favourites,...resposne.data.data],favPages:resposne.data.pageCount})
        })
      }
  
      getOrders = (page) =>{
        axios.get(`${BASE_END_POINT}orders?clientId=${this.props.currentUser.user.id}&page=${page}`)
        .then(resposne=>{
          console.log("ORDERS   ",resposne.data)
          this.setState({orders:[...this.state.orders,...resposne.data.data],ordersPages:resposne.data.pageCount})
        })
      }
    
      step2 = ()  =>{
       const flag = 2;
        this.setState({ flag });
      }

      getCities = () => {
        axios.get(`${BASE_END_POINT}cities`)
        .then(response=>{
          console.log('CITIES  ',response.data)
          this.setState({citiies:response.data})
          this.getAreas(response.data[0].id)
        })
        .catch(error=>{
          console.log('CITIES ERROR  ',error)
          console.log('CITIES ERROR  ',error.respose)
        })
      }

      getAreas = (areaId) => {
        axios.get(`${BASE_END_POINT}cities/${areaId}/areas`)
        .then(response=>{
          console.log('AREAS  ',response.data)
          this.setState({areas:response.data})
        })
        .catch(error=>{
          console.log('AREAS ERROR  ',error)
          console.log('AREAS ERROR  ',error.respose)
        })
      }


      showModal = () => {
        this.setState({
          visible: true,
        });
      }
    
      handleOk = (e) => {
        //console.log(e);
        this.setState({
          visible: false,
        });
      }
    
      handleCancel = (e) => {
       // console.log(e);
        this.setState({
          visible: false,
        });
      }

         
    changePassword = (e) => {
      e.preventDefault();
    var msg = message.loading("ًWAIT")
      this.props.form.validateFieldsAndScroll(['newPassword','confirmPassword'],(err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
        if(values.newPassword==values.confirmPassword){
         var data ={
          newPassword:values.newPassword,
          email:this.props.currentUser.user.email
         }
          axios.post(`${BASE_END_POINT}reset-password`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then(response=>{
            console.log("done   ")
            this.setState({visible:false})
            this.props.form.resetFields()
            msg.then(() => message.success('DONE', 2.5))
              //l.then(() => message.success(allStrings.updatedDone, 2.5))
              //this.props.history.push('/countries') 
          })
          .catch(error=>{
            console.log("error  ", error)
            msg.then(() => message.error('ERROR', 2.5))
          })
        }else{
          msg.then(() => message.error('confirm password must be same password', 2.5))
        }
        }
      });
      
    }

    changeProfileData = (e) => {
      e.preventDefault();
    var msg = message.loading("ًWAIT")
      this.props.form.validateFieldsAndScroll(['firstname','lastname','city','area','email','phone','address'],(err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          var data = new FormData()
          data.append('firstname',values.firstname)
          data.append('lastname',values.lastname)
          data.append('email',values.email)
          data.append('phone',values.phone)
          data.append('area',values.area.key)
          data.append('city',values.city.key)
          data.append('address',values.address)
          data.append('signUpFrom','normal')
          data.append('type','CLIENT')
          data.append('token','')
          if(this.state.image){
            data.append('img',this.state.image);
          }
          axios.put(`${BASE_END_POINT}user/${this.props.currentUser.user.id}/updateInfo`,data,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log("done   ")
            this.setState({editModel:false})
            this.props.form.resetFields()
            msg.then(() => message.success('DONE', 2.5))
            const user = {...this.props.currentUser,user:{...response.data.user}}
            localStorage.setItem('@USER', JSON.stringify(user));  
            this.props.getUser(user);
            
          })
          .catch(error=>{
            console.log("error  ", error.response)
            msg.then(() => message.error('ERROR', 2.5))
          })
        
        }
      });
      
    }
    
  
  
    render() {
        const {currentUser,isRTL} = this.props
        const { getFieldDecorator } = this.props.form;
        const {citiies,areas,flag,favourites,favPages,orders,ordersPages} = this.state
        //select
        const Option = Select.Option;
      return ( 
        <div>
          <div className='profile'>
             <Nav></Nav> 
          </div>
          <MDBContainer> 
            <div className='info'>
               <div className='profilePic'>
                 <img src={this.state.image?URL.createObjectURL(this.state.image):currentUser.user.img?currentUser.user.img : require('../../assets/images/soeqprofileicon.jpg')}></img>
                 <input type='file' className="upImg" onChange={e=>{
                   console.log("FILE    ",e.target.files[0])
                   this.setState({
                     image:e.target.files[0]
                   })
                 }}></input>
                 <img style={{padding: '36px',background: '#000',opacity: '0.9'}} className="cam" src={require('../../assets/images/photo_camera.png')}></img>
               </div>
               <p className='headT'>{allStrings.personalInfo}</p>
               
              <MDBRow>
                  <MDBCol md="3">
                    <p className='colp'>{allStrings.name}</p>
                    <p className='cold'>{currentUser.user.firstname+" "+currentUser.user.lastname}</p>
                  </MDBCol>
                  <MDBCol md="3">
                    <p className='colp'>{allStrings.email}</p>
                    <p className='cold'>{currentUser.user.email} </p>
                  </MDBCol>
                  <MDBCol md="3">
                    <p className='colp'>{allStrings.address}</p>
                    <p className='cold'>{currentUser.user.address}</p>
                  </MDBCol>
                  <MDBCol md="3">
                    <p className='colp'>{allStrings.phone}</p>
                    <p className='cold'>{currentUser.user.phone}</p>
                  </MDBCol>
              </MDBRow> 
              <div className='profBtnG'>
                {this.props.currentUser.user.signUpFrom!='social'&&
                <Button className='profBtn' onClick={this.showModal}>{allStrings.changePass}</Button>
                }
                <Button className='profBtn'  onClick={()=>this.setState({editModel:true})}>{allStrings.changeInfo}</Button>
              </div>
              <br></br>
              <a 
              onClick={()=>{
                localStorage.removeItem('@USER');  
                this.props.getUser(null);
                this.props.history.replace('/Login')
              }}
              style={{color:'#46a0fc'}}>{allStrings.logout}</a>
              <Modal
                title={allStrings.changePass}
                visible={this.state.visible}
                onOk={this.changePassword}
                onCancel={this.handleCancel}
                okText={allStrings.save}
                cancelText={allStrings.cancel}
                >
              
                  <Form onSubmit={this.changePassword} className="login-form">
                  {/*
                  <Form.Item>
                    {getFieldDecorator('currentPassword', {
                        rules: [{ required: true, message: 'Please enter  Password' }],
                    })(
                        <Input.Password placeholder={allStrings.currentPass} />
                    )}
                    </Form.Item>
                    */}
                    <Form.Item>
                    {getFieldDecorator('newPassword', {
                        rules: [{ required: true, 
                          message: isRTL?allStrings.passwordValid:allStrings.passwordValid                         }],
                    })(
                        <Input.Password placeholder={allStrings.newPassword} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('confirmPassword', {
                        rules: [{ required: true, 
                          message: isRTL?allStrings.confirmPasswordValid:allStrings.confirmPasswordValid
                        }],
                    })(
                        <Input.Password placeholder={allStrings.confirmPassword} />
                    )}
                    </Form.Item> 
                  </Form>
                </Modal>

                <Modal
                    title={allStrings.personalInfo}
                    visible={this.state.editModel}
                    onOk={this.changeProfileData}
                    okText={allStrings.save}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setState({editModel:false})}
                >
               
                  <div className='cartStep2'>
                    <Form>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(50% - 17px)' ,marginRight:'12px' }}>
                    {getFieldDecorator('firstname', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.firstnameValid:allStrings.firstnameValid      
                          }, 
                        ],
                        initialValue: currentUser.user.firstname
                    })(
                        <Input placeholder={allStrings.firstname} />
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(50% - 0px)' ,marginLeft:'4px' }}>
                    {getFieldDecorator('lastname', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.lastnameValid:allStrings.lastnameValid                         
                           }, 
                        ],
                        initialValue: currentUser.user.lastname
                    })(
                        <Input placeholder={allStrings.lastname} />
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(50% - 17px)' ,marginRight:'12px' }}>
                    {getFieldDecorator('city', {
                        rules: [{ required: true, 
                          message: isRTL?allStrings.cityValid:allStrings.cityValid  
                        }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.city}
                      style={{ width: '100%'}} 
                      onChange={(value)=>this.getAreas(value.key)}
                      >
                         {citiies.map(city=>(
                            <Option value={city.id}>{isRTL?city.arabicCityName:city.cityName} </Option>
                          ))}    

                      </Select>
                    )}
                    </Form.Item>
                    <Form.Item style={{ display: 'inline-block',paddingRight: '7px', width: 'calc(50% - 0px)' ,marginLeft:'4px' }}>
                      {getFieldDecorator('area', {
                          rules: [{ required: true, 
                            message: isRTL?allStrings.areaValid:allStrings.areaValid  
                          }],
                      })(
                        <Select labelInValue  
                        placeholder={allStrings.area}
                        style={{ width: '100%'}} >
                          {areas.map(area=>(
                            <Option value={area.id}>{isRTL?area.arabicAreaName:area.areaName} </Option>
                          ))}         
                        </Select>
                      )}
                      </Form.Item>
                                                  
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.emailValid:allStrings.emailValid  
                          },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                        initialValue: currentUser.user.email
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [
                          { required: true, 
                            message: isRTL?allStrings.phoneValid:allStrings.phoneValid  
                          },
                          //{type: 'number',  message: 'Please enter numbers' }
                        ],
                        initialValue: currentUser.user.phone
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('address', {
                        rules: [{ required: true, 
                          message: isRTL?allStrings.addressValid:allStrings.addressValid  
                        }],
                        initialValue: currentUser.user.address
                    })(
                        <TextArea rows={2} placeholder={allStrings.address} />
                    )}
                    </Form.Item>  
                    </Form>
                    </div>
                </Modal>
            </div> 
            {flag==1&&
            <div className='favs'>
              <div className="profileNav">
            <ul>
              <li onClick={this.step2}>{allStrings.myOrders}</li>
              <li style={{color:'#000',borderBottom:'2px solid #46a0fc'}} onClick={this.step1}>{allStrings.myFav}</li>
            </ul>
            </div>
              <MDBRow>
              
                {
                  favourites.length>0?
                favourites.map(val=>(
                   <MDBCol md="4">
                   <div className='slideItem' style={{background:" transparent"}}>
                       <NavLink to={`product/${val.product.id}`} >
                       <img style={{width:'100%',height:'250px',margin:'0px'}} src={val.product.img[0]}></img>
                       </NavLink>
                         <div className='love' style={{marginTop: '-50px',right: '25px'}}>
                             <img src={require('../../assets/images/redHeart.png')}></img>
                         </div>
                         <div className='bottom' style={{background:" transparent",color:'#757271'}}>
                             <p className='pname'>{val.product.name}</p>
                             <p className='pname'>{allStrings.by} : {val.product.company}</p>
                             <p style={{color:'#000',fontWeight: '500'}}>{val.product.price} {allStrings.eg}</p>
                         </div>
                   </div>
                 </MDBCol>   
                ))
                :
                  <div style={{width:'100%',textAlign:'center',marginBottom:'50%'}} >
                    {allStrings.noFavourites}
                  </div>

              }  
              </MDBRow>

              {this.favPage<favPages&&
              <Button className='profBtn' onClick={()=>{
                if(this.favPage <= favPages){
                  this.favPage = this.favPage+1
                  this.getFavourites(this.favPage)
                }
              }}>{allStrings.loadMore}</Button>
              }
            </div>
          }

          {flag==2&&
            <div className='favs'>
              <div className="profileNav">
                <ul>
                  <li style={{color:'#000',borderBottom:'2px solid #46a0fc'}} onClick={this.step2}>{allStrings.myOrders}</li>
                  <li onClick={this.step1}>{allStrings.myFav}</li>
                </ul>
              </div>
              <div className="myOrder">
                {
                  orders.length>0?
                orders.map(val=>(
              <MDBRow>
               <div className='orderHeader'>
                  <p className='cold'>{allStrings.orderNumber}: <span> {val.id}</span></p>
                  <p className='cold'>{allStrings.totalCost}: <span> {val.finalTotal} {allStrings.eg}</span></p>
                  <p className='cold'>{allStrings.address}: <span> {val.address} </span></p>
                  <p className='cold'>{allStrings.status}: <span> {val.status}</span></p>
                  <img style={{float:'right'}} src={require('../../assets/images/offline_pin.png')}></img>
                </div>
                <div className='orderBody'>
                  <MDBRow>
                   {val.productOrders.map(data=>(
                      <MDBCol md="12" lg="6">
                      <div className='myPart' style={{width:'80%'}}>
                          <NavLink to={`product/${data.product.id}`}>
                              <img width={150} height={150} src={data.product.img[0]}></img>
                          </NavLink>
                          <div className="myPartDec">
                            <div>
                            <p style={{display:'inline-block'}}>{data.product.name}</p>
                            <span style={{fontWeight:' 500',float:'right'}}>{data.product.hasOffer?data.product.offerPrice:data.product.price} {allStrings.eg}</span>
                            </div>
                          
                          <br></br>
                          <Rater rating={parseInt(data.product.ratePrecent, 10)} total={5} interactive={false} />
                          <span> (<span className='totalRate'>{data.product.rateNumbers}</span>)</span>
                          <br></br>
                          <div style={{width:'100%'}}>
                            <span className='stock stockQ' style={{color:'#9ba3b6',fontWeight:400}}>{allStrings.only}  {data.product.quantity}  {allStrings.leftin}</span>
                          </div>
                          <div style={{width:'100%',textAlign: 'right'}}>
                            <p className='qtySpan' style={{fontWeight: '500'}}>{allStrings.QTY}  </p>
                            <input style={{color: '#000',fontWeight: '400'}} type='number' className='numberInput' value={data.count} disabled></input>
                          </div>
                          </div>
                      </div>
                      </MDBCol>
                   ))}              
                  </MDBRow>

                </div>
              </MDBRow>
                ))
                :
                <div style={{width:'100%',textAlign:'center',marginBottom:'50%'}} >
                  {allStrings.noOrders}
                </div>
              }
              {this.ordersPage<ordersPages&&
              <Button className='profBtn' onClick={()=>{
                if(this.ordersPage <= ordersPages){
                  this.ordersPage = this.ordersPage+1
                  this.getOrders(this.ordersPage)
                }
              }}>{allStrings.loadMore}</Button>
              }
              </div>
             { /* div order*/ }
            </div>
          }
          </MDBContainer>
          <Footer></Footer>

      </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    getUser
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Profile = Form.create({ name: 'normal_login' })(Profile));
